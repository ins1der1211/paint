package ua.dp.ins1der.paint.data.remote;

import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.mockwebserver.Dispatcher;
import com.squareup.okhttp.mockwebserver.MockResponse;
import com.squareup.okhttp.mockwebserver.MockWebServer;
import com.squareup.okhttp.mockwebserver.RecordedRequest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import rx.observers.TestSubscriber;
import ua.dp.ins1der.paint.BaseRobolectricTest;
import ua.dp.ins1der.paint.data.remote.model.RegisterRequest;
import ua.dp.ins1der.paint.data.remote.model.RegisterResponse;

import static org.mockito.Matchers.any;

/**
 * Created by Yurchenko on 19.11.2016.
 */

public class PaintServiceTest extends BaseRobolectricTest {

    MockWebServer mockWebServer;
    PaintService paintService;

    @Before
    public void setUp() throws IOException {
        super.setUp();
        mockWebServer = new MockWebServer();
        mockWebServer.start();

        final Dispatcher dispatcher = new Dispatcher() {
            @Override
            public MockResponse dispatch(RecordedRequest request) throws InterruptedException {
                if(request.getPath().equals("/register")) {
                    return new MockResponse().setResponseCode(200)
                            .setBody(mTestUtils.readString("register_response.json"));
                }
                else if(request.getPath().equals("/upload")) {
                    return new MockResponse().setResponseCode(200);
                }
                return new MockResponse().setResponseCode(404);
            }
        };

        mockWebServer.setDispatcher(dispatcher);
        HttpUrl baseUrl = mockWebServer.url("/");
        paintService = PaintService.Factory.makePaintService(baseUrl.toString());
    }

    @Test
    public void registerTest() {
        TestSubscriber<RegisterResponse> testSubscriber = new TestSubscriber<>();
        paintService.register(new RegisterRequest("aaa", "bbb", "ccc")).subscribe(testSubscriber);

        testSubscriber.assertNoErrors();
        testSubscriber.assertValueCount(1);
    }

    @Test
    public void uploadTest() {
    }

    @After
    public void tearDown() throws Exception {
        mockWebServer.shutdown();
    }
}
