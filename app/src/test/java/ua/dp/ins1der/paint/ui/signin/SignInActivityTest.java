package ua.dp.ins1der.paint.ui.signin;

import android.content.Intent;
import android.support.percent.PercentRelativeLayout;
import android.widget.EditText;
import android.widget.ImageView;

import ua.dp.ins1der.paint.BaseRobolectricTest;
import ua.dp.ins1der.paint.R;
import ua.dp.ins1der.paint.ui.main.MainActivity;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.verify;
import static org.robolectric.Shadows.shadowOf;

/**
 * Created by Yurchenko on 16.11.2016.
 */

public class SignInActivityTest extends BaseRobolectricTest {

    @Mock SignInPresenter mockSignInPresenter;
    @InjectMocks SignInActivity mSignInActivity;

    SignInActivity signInActivity;

    @BindView(R.id.sa_et_email) EditText emailET;
    @BindView(R.id.sa_et_password) EditText passwordET;
    @BindView(R.id.sa_iv_google_sign) ImageView googleButton;
    @BindView(R.id.for_test) PercentRelativeLayout relativeLayout;

    String goodEmail = "ins1der12111991@gmail.com";
    String goodPassword = "password";

    @Before
    public void setUp() {
        signInActivity = Robolectric.setupActivity(SignInActivity.class);
        ButterKnife.bind(this, signInActivity);
    }

    @Test
    public void getEmailTest() {
        emailET.setText(goodEmail);
        assertThat(emailET.getText().toString()).isEqualTo(signInActivity.getEmail());
    }

    @Test
    public void getPasswordTest() {
        passwordET.setText(goodPassword);
        assertThat(passwordET.getText().toString()).isEqualTo(signInActivity.getPassword());
    }

    @Test
    public void startPaintActivityTest() {
        relativeLayout.performClick();
        Intent expected = MainActivity.getStartIntent(signInActivity);
        assertThat(shadowOf(signInActivity).getNextStartedActivity().toString()).isEqualTo(expected.toString());
    }

    @Test
    public void onDestroyTest() {
        mSignInActivity = Robolectric.setupActivity(SignInActivity.class);
        MockitoAnnotations.initMocks(this);
        mSignInActivity.onDestroy();
        verify(mockSignInPresenter).detachView();
    }
}
