package ua.dp.ins1der.paint;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import rx.Observable;
import ua.dp.ins1der.paint.data.DataManager;
import ua.dp.ins1der.paint.data.local.DatabaseHelper;
import ua.dp.ins1der.paint.data.local.FileHelper;
import ua.dp.ins1der.paint.data.local.PreferencesHelper;
import ua.dp.ins1der.paint.data.local.model.Image;
import ua.dp.ins1der.paint.data.remote.PaintService;
import ua.dp.ins1der.paint.util.EventPosterHelper;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.robolectric.Robolectric;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.internal.Shadow;

import java.io.File;
import java.io.IOException;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * Created by Yurchenko on 17.11.2016.
 */

public class DataManagerTest extends BaseRobolectricTest {

    @Mock PaintService mockPaintService;
    @Mock DatabaseHelper mockDatabaseHelper;
    @Mock PreferencesHelper mockPreferencesHelper;
    @Mock EventPosterHelper mockEventPosterHelper;
    @Mock FileHelper mockFileHelper;

    DataManager dataManager;


    @Before
    public void setUp() throws IOException {
        super.setUp();
        dataManager = new DataManager(mockPaintService, mockDatabaseHelper,
                mockPreferencesHelper, mockEventPosterHelper, mockFileHelper);
    }

    @Test
    public void isUserLoggedInTest() {
        when(mockPreferencesHelper.isUserLoggedIn()).thenReturn(true);
        assertTrue(dataManager.isUserLoggedIn());
        when(mockPreferencesHelper.isUserLoggedIn()).thenReturn(false);
        assertFalse(dataManager.isUserLoggedIn());
    }

    @Test
    public void saveBitmapTest() throws IOException {
        Bitmap bitmap = BitmapFactory.decodeResource(RuntimeEnvironment.application.getResources(), R.drawable.ic_menu_send);
        when(mockFileHelper.saveBitmap(bitmap)).thenReturn(Observable.create(s -> {
            try {
                s.onNext(mTestUtils.readFile("test.png"));
                s.onCompleted();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }));
        dataManager.saveBitmap(bitmap);
        verify(mockFileHelper).saveBitmap(bitmap);
        verify(mockDatabaseHelper).saveBitmap(any(Image.class));
        verifyNoMoreInteractions(mockFileHelper);
        verifyNoMoreInteractions(mockDatabaseHelper);
    }

    @Test
    public void getImagesForGalleryTest() {
        Image image1 = new Image();
        image1.setDeleted(1);
        Image image2 = new Image();
        image2.setDeleted(0);
        Image image3 = new Image();
        image3 .setDeleted(0);
        when(mockDatabaseHelper.getImageList()).thenReturn(Observable.create(
           subscriber -> {
               subscriber.onNext(image1);
               subscriber.onNext(image2);
               subscriber.onNext(image3);
               subscriber.onCompleted();
           }
        ));
        dataManager.getImagesForGallery()
                .subscribe(
                        imageList -> assertEquals(2, imageList.size())
                );
    }

}
