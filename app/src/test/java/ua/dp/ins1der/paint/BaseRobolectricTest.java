package ua.dp.ins1der.paint;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.io.IOException;

/**
 * Created by Yurchenko on 17.11.2016.
 */

@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE,
        constants = BuildConfig.class,
        sdk = 23)
public class BaseRobolectricTest extends BaseTest {

    @Before
    public void setUp() throws IOException {
        super.setUp();
    }

    @Test
    public void emptyTest() {}
}
