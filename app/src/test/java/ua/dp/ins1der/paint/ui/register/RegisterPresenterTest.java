package ua.dp.ins1der.paint.ui.register;

import ua.dp.ins1der.paint.BaseTest;
import ua.dp.ins1der.paint.data.DataManager;
import ua.dp.ins1der.paint.data.remote.model.RegisterRequest;
import ua.dp.ins1der.paint.data.remote.model.RegisterResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import rx.Observable;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;


/**
 * Created by Yurchenko on 17.11.2016.
 */

@RunWith(MockitoJUnitRunner.class)
public class RegisterPresenterTest extends BaseTest {

    @Mock
    RegisterMvpView mockRegisterMvpView;
    @Mock
    DataManager mockDataManager;
    @Mock
    RegisterResponse mockRegisterResponse;

    private RegisterPresenter mRegisterPresenter;

    @Before
    public void setUp() {
        mRegisterPresenter = new RegisterPresenter(mockDataManager);
        mRegisterPresenter.attachView(mockRegisterMvpView);
    }

    @After
    public void tearDown() {
        mRegisterPresenter.detachView();
    }

    @Test
    public void registerSuccesTest() {
        when(mockRegisterMvpView.getEmail()).thenReturn("aaa");
        when(mockRegisterMvpView.getPassword()).thenReturn("aaa");
        when(mockRegisterMvpView.getPasswordRepeat()).thenReturn("aaa");
        when(mockDataManager.register(any(RegisterRequest.class))).thenReturn(Observable.just(mockRegisterResponse));

        mRegisterPresenter.onRegisterClick();
        verify(mockRegisterMvpView).showProgress();
        verify(mockRegisterMvpView).getEmail();
        verify(mockRegisterMvpView).getPassword();
        verify(mockRegisterMvpView).getPasswordRepeat();
        verify(mockDataManager).register(any(RegisterRequest.class));
        verify(mockRegisterMvpView).hideProgress();
        verify(mockRegisterMvpView).startMainActivity();
        verifyNoMoreInteractions(mockDataManager, mockRegisterMvpView);
    }

    @Test
    public void passwordsNotMatchTest() {
        when(mockRegisterMvpView.getEmail()).thenReturn("aaa");
        when(mockRegisterMvpView.getPassword()).thenReturn("aaa");
        when(mockRegisterMvpView.getPasswordRepeat()).thenReturn("bbb");

        mRegisterPresenter.onRegisterClick();
        verify(mockRegisterMvpView).showProgress();
        verify(mockRegisterMvpView).getEmail();
        verify(mockRegisterMvpView).getPassword();
        verify(mockRegisterMvpView).getPasswordRepeat();
        verify(mockRegisterMvpView).showPasswordsNotMatch();
        verify(mockRegisterMvpView).hideProgress();
        verifyNoMoreInteractions(mockDataManager, mockRegisterMvpView);
    }

    @Test
    public void registerFailTest() {
        when(mockRegisterMvpView.getEmail()).thenReturn("aaa");
        when(mockRegisterMvpView.getPassword()).thenReturn("aaa");
        when(mockRegisterMvpView.getPasswordRepeat()).thenReturn("aaa");
        when(mockDataManager.register(any(RegisterRequest.class))).thenReturn(Observable.error(new Throwable()));

        mRegisterPresenter.onRegisterClick();
        verify(mockRegisterMvpView).showProgress();
        verify(mockRegisterMvpView).getEmail();
        verify(mockRegisterMvpView).getPassword();
        verify(mockRegisterMvpView).getPasswordRepeat();
        verify(mockDataManager).register(any(RegisterRequest.class));
        verify(mockRegisterMvpView).showRegisterFailed(anyString());
        verify(mockRegisterMvpView).hideProgress();
        verifyNoMoreInteractions(mockDataManager, mockRegisterMvpView);
    }

}
