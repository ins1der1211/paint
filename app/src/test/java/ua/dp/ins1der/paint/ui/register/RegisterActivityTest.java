package ua.dp.ins1der.paint.ui.register;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import ua.dp.ins1der.paint.BaseRobolectricTest;
import ua.dp.ins1der.paint.ui.main.MainActivity;

import org.junit.Before;
import org.junit.Test;

import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.robolectric.Robolectric;
import org.robolectric.RuntimeEnvironment;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.google.common.truth.Truth.assertThat;
import static junit.framework.Assert.assertTrue;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;
import static org.robolectric.Shadows.shadowOf;

/**
 * Created by Yurchenko on 17.11.2016.
 */

public class RegisterActivityTest extends BaseRobolectricTest {

    RegisterActivity registerActivity;

    @BindView(ua.dp.ins1der.paint.R.id.ra_root) View root;
    @BindView(ua.dp.ins1der.paint.R.id.ra_et_email) EditText emailET;
    @BindView(ua.dp.ins1der.paint.R.id.ra_et_password) EditText passwordET;
    @BindView(ua.dp.ins1der.paint.R.id.ra_et_password_repeat) EditText passwordRepeatET;
    @BindView(ua.dp.ins1der.paint.R.id.ra_pb) ProgressBar progressBar;
    @BindView(ua.dp.ins1der.paint.R.id.ra_b_register) Button registerB;

    String email = "ins1der12111991@gmail.com";
    String pass = "password";
    String passRepeat = "password";

    @Before
    public void setUp() {
        registerActivity = Robolectric.setupActivity(RegisterActivity.class);
        ButterKnife.bind(this, registerActivity);
    }

    @Test
    public void getEmailTest() {
        emailET.setText(email);
        assertThat(email).isEqualTo(registerActivity.getEmail());
    }

    @Test
    public void getPassTest() {
        passwordET.setText(pass);
        assertThat(pass).isEqualTo(registerActivity.getPassword());
    }

    @Test
    public void getPassRepeatTest() {
        passwordRepeatET.setText(passRepeat);
        assertThat(passRepeat).isEqualTo(registerActivity.getPasswordRepeat());
    }

    @Test
    public void showProgressTest() {
        registerActivity.showProgress();
        assertTrue(progressBar.getVisibility() == View.VISIBLE);
        assertTrue(registerB.getVisibility() == View.INVISIBLE);
    }

    @Test
    public void hideProgressTest() {
        registerActivity.hideProgress();
        assertTrue(progressBar.getVisibility() == View.INVISIBLE);
        assertTrue(registerB.getVisibility() == View.VISIBLE);
    }

    @Test
    public void startMainActivityTest() {
        registerActivity.startMainActivity();
        Intent expected = new Intent(registerActivity, MainActivity.class);
        assertThat(shadowOf(registerActivity).getNextStartedActivity().toString()).isEqualTo(expected.toString());
    }

    @Test
    public void getStartIntentTest() {
        Intent expected = new Intent(RuntimeEnvironment.application, RegisterActivity.class);
        assertTrue(expected.toString().equals(RegisterActivity.getStartIntent(RuntimeEnvironment.application).toString()));
    }
}
