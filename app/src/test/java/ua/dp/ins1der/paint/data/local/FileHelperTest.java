package ua.dp.ins1der.paint.data.local;

import android.graphics.Bitmap;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.util.List;

import rx.observers.TestSubscriber;
import ua.dp.ins1der.paint.BaseTest;
import ua.dp.ins1der.paint.util.FileUtils;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.when;


/**
 * Created by Yurchenko on 18.11.2016.
 */


@RunWith(MockitoJUnitRunner.class)
public class FileHelperTest extends BaseTest {

    @Rule public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock FileUtils mockFileUtils;
    @Mock Bitmap mockBitmap;

    @InjectMocks FileHelper fileHelper;

    File[] files;
    TestSubscriber testSubscriber;

    @Before
    public void setUp() throws IOException {
        super.setUp();
        MockitoAnnotations.initMocks(fileHelper);
        files = new File[5];
        for(int i = 0; i < files.length; i++) {
            files[i] = mTestUtils.readFile("test.png");
        }
        testSubscriber = new TestSubscriber();
    }

    @Test
    public void saveBitmapTest() throws IOException {
        when(mockFileUtils.getFileToSave()).thenReturn(mTestUtils.readFile(""));
        fileHelper.saveBitmap(mockBitmap);
    }

    @Test
    public void deleteFileTest() throws IOException {
        File file = File.createTempFile("xxx", "ccc");
        fileHelper.deleteFile(file).subscribe(testSubscriber);
        testSubscriber.assertNoErrors();
        testSubscriber.assertCompleted();
        testSubscriber.assertValue(Boolean.valueOf(true));
    }
}