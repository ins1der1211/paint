package ua.dp.ins1der.paint.ui.signin;

import ua.dp.ins1der.paint.BaseTest;
import ua.dp.ins1der.paint.data.DataManager;
import ua.dp.ins1der.paint.data.remote.model.SignInRequest;
import ua.dp.ins1der.paint.data.remote.model.SignInResponse;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import rx.Observable;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * Created by Yurchenko on 17.11.2016.
 */

@RunWith(MockitoJUnitRunner.class)
public class SignInPresenterTest extends BaseTest {

    @Mock SignInMvpView mockSignInMvpView;
    @Mock DataManager mockDataManager;
    @Mock SignInResponse mockSignInResponse;
    @Mock GoogleSignInResult mockGoogleSignInResult;
    @Mock GoogleSignInAccount mockGoogleSignInAccount;

    private SignInPresenter signInPresenter;

    @Before
    public void setUp() {
        signInPresenter = new SignInPresenter(mockDataManager);
        signInPresenter.attachView(mockSignInMvpView);
    }

    @After
    public void tearDown() {
        signInPresenter.detachView();
    }

    @Test
    public void signInSuccessTest() {
        when(mockSignInMvpView.getEmail()).thenReturn("aaa");
        when(mockSignInMvpView.getPassword()).thenReturn("bbb");
        when(mockDataManager.signin(any(SignInRequest.class))).thenReturn(Observable.just(mockSignInResponse));
        signInPresenter.onSigninClick();

        verify(mockSignInMvpView).getEmail();
        verify(mockSignInMvpView).getPassword();
        verify(mockSignInMvpView).startMainActivity();
        verify(mockDataManager).signin(any(SignInRequest.class));
        verifyNoMoreInteractions(mockSignInMvpView, mockDataManager);
    }

    @Test
    public void emailEmptyTest() {
        when(mockSignInMvpView.getEmail()).thenReturn("");
        when(mockSignInMvpView.getPassword()).thenReturn("bbb");
        when(mockDataManager.signin(any(SignInRequest.class))).thenReturn(Observable.just(mockSignInResponse));
        signInPresenter.onSigninClick();

        verify(mockSignInMvpView).getEmail();
        verify(mockSignInMvpView).getPassword();
        verify(mockSignInMvpView).signinFail(ua.dp.ins1der.paint.R.string.signin_error);
        verifyNoMoreInteractions(mockSignInMvpView, mockDataManager);
    }

    @Test
    public void passwordEmptyTest() {
        when(mockSignInMvpView.getEmail()).thenReturn("aaa");
        when(mockSignInMvpView.getPassword()).thenReturn("");
        when(mockDataManager.signin(any(SignInRequest.class))).thenReturn(Observable.just(mockSignInResponse));
        signInPresenter.onSigninClick();

        verify(mockSignInMvpView).getEmail();
        verify(mockSignInMvpView).getPassword();
        verify(mockSignInMvpView).signinFail(ua.dp.ins1der.paint.R.string.signin_error);
        verifyNoMoreInteractions(mockSignInMvpView, mockDataManager);
    }

    @Test
    public void signInFailTest() {
        when(mockSignInMvpView.getEmail()).thenReturn("aaa");
        when(mockSignInMvpView.getPassword()).thenReturn("bbb");
        when(mockDataManager.signin(any(SignInRequest.class))).thenReturn(Observable.error(new Throwable()));
        signInPresenter.onSigninClick();

        verify(mockSignInMvpView).getEmail();
        verify(mockSignInMvpView).getPassword();
        verify(mockDataManager).signin(any(SignInRequest.class));
        verify(mockSignInMvpView).signinFail(ua.dp.ins1der.paint.R.string.signin_error);
        verifyNoMoreInteractions(mockSignInMvpView, mockDataManager);
    }

    @Test
    public void googleSignInResultFailTest() {
        when(mockGoogleSignInResult.isSuccess()).thenReturn(false);
        signInPresenter.handleGoogleSignResult(mockGoogleSignInResult);

        verify(mockSignInMvpView).signinFail(ua.dp.ins1der.paint.R.string.signin_error);
        verifyNoMoreInteractions(mockSignInMvpView, mockDataManager);
    }

    @Test
    public void googleSignInSuccessTest() {
        when(mockGoogleSignInResult.isSuccess()).thenReturn(true);
        when(mockGoogleSignInResult.getSignInAccount()).thenReturn(mockGoogleSignInAccount);
        when(mockGoogleSignInAccount.getEmail()).thenReturn("email");
        when(mockDataManager.signin(any(SignInRequest.class))).thenReturn(Observable.just(mockSignInResponse));

        signInPresenter.handleGoogleSignResult(mockGoogleSignInResult);

        verify(mockGoogleSignInResult).isSuccess();
        verify(mockSignInMvpView, never()).signinFail(ua.dp.ins1der.paint.R.string.signin_error);
        verify(mockGoogleSignInResult).getSignInAccount();
        verify(mockDataManager).signin(any(SignInRequest.class));
        verify(mockGoogleSignInAccount).getEmail();
        verify(mockSignInMvpView).startMainActivity();
        verifyNoMoreInteractions(mockGoogleSignInResult, mockGoogleSignInAccount, mockDataManager, mockSignInMvpView);
    }

    @Test
    public void googleSignInFailTest() {
        when(mockGoogleSignInResult.isSuccess()).thenReturn(true);
        when(mockGoogleSignInResult.getSignInAccount()).thenReturn(mockGoogleSignInAccount);
        when(mockGoogleSignInAccount.getEmail()).thenReturn("email");
        when(mockDataManager.signin(any(SignInRequest.class))).thenReturn(Observable.error(new Throwable()));

        signInPresenter.handleGoogleSignResult(mockGoogleSignInResult);

        verify(mockGoogleSignInResult).isSuccess();
        verify(mockGoogleSignInResult).getSignInAccount();
        verify(mockDataManager).signin(any(SignInRequest.class));
        verify(mockGoogleSignInAccount).getEmail();
        verify(mockSignInMvpView).signinFail(ua.dp.ins1der.paint.R.string.signin_error);
        verifyNoMoreInteractions(mockGoogleSignInResult, mockGoogleSignInAccount, mockDataManager, mockSignInMvpView);
    }

}
