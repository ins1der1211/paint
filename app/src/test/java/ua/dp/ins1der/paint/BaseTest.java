package ua.dp.ins1der.paint;

import ua.dp.ins1der.paint.test_utils.RxSchedulersOverrideRule;
import ua.dp.ins1der.paint.test_utils.TestUtils;

import org.junit.Before;
import org.junit.Rule;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.IOException;

/**
 * Created by Yurchenko on 16.11.2016.
 */

public class BaseTest {

    public TestUtils mTestUtils;

    @Before
    public void setUp() throws IOException {
        mTestUtils = new TestUtils();
    }

    @Rule public final RxSchedulersOverrideRule mSchedulersOverrideRule = new RxSchedulersOverrideRule();

    @Rule public MockitoRule mockitoRule = MockitoJUnit.rule();
}
