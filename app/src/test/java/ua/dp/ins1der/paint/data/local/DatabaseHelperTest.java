package ua.dp.ins1der.paint.data.local;

import android.database.Cursor;

import org.junit.Before;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import java.io.File;
import java.io.IOException;
import java.util.List;

import rx.observers.TestSubscriber;
import ua.dp.ins1der.paint.BaseRobolectricTest;
import ua.dp.ins1der.paint.data.local.mappers.FileToImageMapper;
import ua.dp.ins1der.paint.data.local.model.Image;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * Created by Yurchenko on 19.11.2016.
 */

public class DatabaseHelperTest extends BaseRobolectricTest {

    File file;
    Image image;

    final DatabaseHelper databaseHelper = new DatabaseHelper(new DbOpenHelper(RuntimeEnvironment.application));


    @Before
    public void setUp() throws IOException {
        super.setUp();
        databaseHelper.clearTables().subscribe();
        file = mTestUtils.readFile("test.png");
        image = new Image();
        image.setImagePath(file.getAbsolutePath());
        image.setSync(1);
    }

    @Test
    public void saveBitmapTest() {
        TestSubscriber testSubscriber = new TestSubscriber();
        databaseHelper.saveBitmap(new Image()).subscribe(testSubscriber);
        testSubscriber.assertNoErrors();
        testSubscriber.assertCompleted();

        databaseHelper.saveBitmap(image)
                .subscribe(
                        value -> {
                            Cursor cursor = databaseHelper.getBriteDb().query("SELECT " + Db.ImagesTable.COLUMN_IMAGE +
                                            ", " + Db.ImagesTable.COLUMN_SYNC +
                                            " FROM " + Db.ImagesTable.TABLE_NAME +
                                            " WHERE " + Db.ImagesTable._ID + " =? ",
                                    String.valueOf(value));
                            cursor.moveToFirst();
                            assertEquals(file.getAbsolutePath(), cursor.getString(cursor.getColumnIndex(Db.ImagesTable.COLUMN_IMAGE)));
                            assertEquals("1", cursor.getString(cursor.getColumnIndex(Db.ImagesTable.COLUMN_SYNC)));
                        }
                );
    }

    @Test
    public void toContentValuesTest() {
        assertEquals(Db.ImagesTable.toContentValues(image).get(Db.ImagesTable.COLUMN_IMAGE), image.getImagePath());
        assertEquals("1", Db.ImagesTable.toContentValues(image).get(Db.ImagesTable.COLUMN_SYNC));
    }

    @Test
    public void populateContentTest() {
        image.setDeleted(0);
        image.setServerId("0");
        databaseHelper.saveBitmap(image)
                .subscribe(
                        value -> {
                            Cursor cursor = databaseHelper.getBriteDb().query("SELECT " +
                                            Db.ImagesTable.COLUMN_IMAGE + ", " +
                                            Db.ImagesTable.COLUMN_SYNC + ", " +
                                            Db.ImagesTable.COLUMN_DELETED + ", " +
                                            Db.ImagesTable.COLUMN_SERVER_ID +
                                            " FROM " + Db.ImagesTable.TABLE_NAME +
                                            " WHERE " + Db.ImagesTable._ID + " =? ",
                                    String.valueOf(value));
                            cursor.moveToFirst();
                            Image image = Db.ImagesTable.populateContent(cursor);
                            assertEquals(image.getImagePath(), file.getAbsolutePath());
                            assertEquals(image.getSync(), 1);
                            assertEquals(image.getDeleted(), 0);
                            assertEquals(image.getServerId(), "0");
                        }
                );
    }

    @Test
    public void getNotSyncImagesTest() {
        TestSubscriber testSubscriber = new TestSubscriber();
        databaseHelper.saveBitmap(image).subscribe();
        databaseHelper.saveBitmap(image).subscribe();
        databaseHelper.saveBitmap(image).subscribe();
        databaseHelper.getNotSyncImages().subscribe(testSubscriber);
        testSubscriber.assertNoErrors();
        testSubscriber.assertCompleted();

        databaseHelper.getNotSyncImages()
                .toList()
                .subscribe(
                        images -> assertTrue(images.size() == 3)
                );
    }

    @Test
    public void markAsSyncTest() {
        image.setServerId("24");
        databaseHelper.saveBitmap(image).subscribe();
        databaseHelper.markAsSync(image).subscribe(
                quantity -> {
                    assertTrue(quantity == 1);
                }
        );
    }

    @Test
    public void deleteTest() {
        image.setServerId("24");
        databaseHelper.saveBitmap(image).subscribe();
        databaseHelper.markAsSync(image).subscribe();
        databaseHelper.delete(image).subscribe(
                number -> assertTrue(number == 1)
        );
        image.setServerId("23");
        databaseHelper.delete(image).subscribe(
                number -> assertTrue(number == 0)
        );
    }

    @Test
    public void getImageListTest() {
        databaseHelper.saveBitmap(image).subscribe();
        databaseHelper.saveBitmap(image).subscribe();
        databaseHelper.saveBitmap(image).subscribe();
        databaseHelper.getImageList()
                .toList()
                .subscribe(
                        imageList -> assertEquals(3, imageList.size())
                );
    }

    @Test
    public void markAsDeletedTest() {
        databaseHelper.saveBitmap(image).subscribe();
        databaseHelper.markAsDeleted(image)
                .subscribe(
                        count -> assertTrue(count == 1)
                );
    }
}
