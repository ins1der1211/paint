package ua.dp.ins1der.paint.ui.paint;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.robolectric.Robolectric;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.util.ActivityController;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.dp.ins1der.paint.BaseRobolectricTest;
import ua.dp.ins1der.paint.R;
import ua.dp.ins1der.paint.ui.widgets.DrawingView;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.robolectric.Shadows.shadowOf;

/**
 * Created by Yurchenko on 22.11.2016.
 */

public class PaintActivityTest extends BaseRobolectricTest {

    @Mock PaintPresenter mockPaintPresenter;

    PaintActivity paintActivity;

    @BindView(R.id.pa_drap) DrawingView drawingView;
    @BindView(R.id.fab_choose_color) FloatingActionButton chooseColorFab;
    @BindView(R.id.fab_erase) FloatingActionButton eraseFab;
    @BindView(R.id.fab_thikness) FloatingActionButton thiknessFab;
    @BindView(R.id.fab_rollback) FloatingActionButton rollBackFab;
    @BindView(R.id.fab_scale) FloatingActionButton scaleFab;
    @BindView(R.id.pa_fab_open_menu) FloatingActionButton openMenuFab;

    @Before
    public void setUp() {
        paintActivity = Robolectric.setupActivity(PaintActivity.class);
        paintActivity.mPaintPresenter = mockPaintPresenter;
        ButterKnife.bind(this, paintActivity);
    }

    @Test
    public void getStartIntentTest() {
        Intent expected = new Intent(RuntimeEnvironment.application, PaintActivity.class);
        assertTrue(expected.toString().equals(PaintActivity.getStartIntent(RuntimeEnvironment.application).toString()));
    }

    @Test
    public void recreateTest() {
        paintActivity.recreate();
        verify(mockPaintPresenter).onSaveInstanceState(any(Bundle.class));
        verify(mockPaintPresenter).detachView();
    }

    @Test
    public void onRestoreInstanceStateTest() {
        paintActivity = Robolectric.buildActivity(PaintActivity.class).create().get();
        paintActivity.mPaintPresenter = mockPaintPresenter;
        paintActivity.onRestoreInstanceState(any(Bundle.class));
        verify(mockPaintPresenter).onRestoreInstanceState(any(Bundle.class));
    }

    @Test
    public void onBackPressedTest() {
        paintActivity.onBackPressed();
        assertNotNull(ShadowAlertDialog.getLatestAlertDialog());
    }

    @Test
    public void saveBitmapTest() {
        paintActivity.onBackPressed();
        assertNotNull(ShadowAlertDialog.getLatestAlertDialog());
        ShadowAlertDialog.getLatestAlertDialog().getButton(DialogInterface.BUTTON_POSITIVE).performClick();
        verify(mockPaintPresenter).savePaint(any(Bitmap.class));
        assertTrue(shadowOf(paintActivity).isFinishing());
    }

    @Test
    public void notSaveBitmapTest() {
        paintActivity.onBackPressed();
        assertNotNull(ShadowAlertDialog.getLatestAlertDialog());
        ShadowAlertDialog.getLatestAlertDialog().getButton(DialogInterface.BUTTON_NEGATIVE).performClick();
        verifyNoMoreInteractions(mockPaintPresenter);
        assertTrue(shadowOf(paintActivity).isFinishing());
    }

    @Test
    public void onDestroyTest() {
        paintActivity.onDestroy();
        verify(mockPaintPresenter).detachView();
    }

    @Test
    public void saveBitmapTest2() {

    }
}
