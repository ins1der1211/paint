package ua.dp.ins1der.paint.ui.gallery;

import android.graphics.Bitmap;
import android.os.Bundle;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import ua.dp.ins1der.paint.BaseTest;
import ua.dp.ins1der.paint.data.DataManager;
import ua.dp.ins1der.paint.data.local.model.Image;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Yurchenko on 11.12.2016.
 */

@RunWith(MockitoJUnitRunner.class)
public class GalleryPresenterTest extends BaseTest {

    @Mock GalleryMvpView mockGalleryMvpView;

    @Mock DataManager mockDataManager;

    @Mock Bundle mockBundle;

    List<Image> imageList;

    private GalleryPresenter mGalleryPresenter;

    @Before
    public void setUp() throws IOException {
        super.setUp();
        mGalleryPresenter = new GalleryPresenter(mockDataManager);
        mGalleryPresenter.attachView(mockGalleryMvpView);

        imageList = new ArrayList<>();
        Image image1 = new Image();
        Image image2 = new Image();
        Image image3 = new Image();
        image1.setImagePath(mTestUtils.readFile("test.png").getAbsolutePath());
        image2.setImagePath(mTestUtils.readFile("test.png").getAbsolutePath());
        image3.setImagePath(mTestUtils.readFile("test.png").getAbsolutePath());
        imageList.add(image1);
        imageList.add(image2);
        imageList.add(image3);

        when(mockDataManager.getImagesForGallery()).thenReturn(Observable.create(
                subscriber -> {
                    subscriber.onNext(imageList);
                    subscriber.onCompleted();
                }
        ));
    }

    @After
    public void tearDown() {
        mGalleryPresenter.detachView();
    }

    @Test
    public void getImagesForGalleryTest() {
        mGalleryPresenter.getImagesForGallery();
        verify(mockGalleryMvpView).initViewPager(anyListOf(Bitmap.class));
        verify(mockGalleryMvpView).initRecyclerView(anyListOf(Bitmap.class));
        assertTrue(mGalleryPresenter.savedInstanceState == null);
    }

    @Test
    public void getImageListTest() {
        mGalleryPresenter.getImagesForGallery();
        assertEquals(imageList, mGalleryPresenter.getImageList());
    }

    @Test
    public void deleteImageTest() {
        mGalleryPresenter.getImagesForGallery();
        when(mockDataManager.deleteFromFiles(mGalleryPresenter.getImageList().get(0)))
                .thenReturn(Observable.create(
                        subscriber -> {
                            subscriber.onNext(true);
                            subscriber.onCompleted();
                        }
                ));

        when(mockDataManager.deleteFromFiles(mGalleryPresenter.getImageList().get(1)))
                .thenReturn(Observable.create(
                        subscriber -> {
                            subscriber.onNext(false);
                            subscriber.onCompleted();
                        }
                ));

        mGalleryPresenter.deleteImage(0);
        verify(mockDataManager).deleteFromFiles(mGalleryPresenter.getImageList().get(0));
        mGalleryPresenter.deleteImage(1);
        verify(mockDataManager).deleteFromFiles(mGalleryPresenter.getImageList().get(1));
        verify(mockDataManager, never()).deleteFromFiles(mGalleryPresenter.getImageList().get(2));

    }

    @Test
    public void onSaveInstanceStatePagerTest() {
        when(mockGalleryMvpView.pagerOpen()).thenReturn(true);
        when(mockGalleryMvpView.pagerPosition()).thenReturn(100);
        mGalleryPresenter.onSaveInstanceState(mockBundle);
        verify(mockBundle).putInt(mGalleryPresenter.LAST_SCREEN, mGalleryPresenter.PAGER_LAST);
        verify(mockBundle).putInt(mGalleryPresenter.LAST_PAGER_POSITION, 100);
    }

    @Test
    public void onSaveInstanceStateGridTest() {
        when(mockGalleryMvpView.pagerOpen()).thenReturn(false);
        when(mockGalleryMvpView.gridType()).thenReturn(true);
        mGalleryPresenter.onSaveInstanceState(mockBundle);
        verify(mockBundle).putInt(mGalleryPresenter.LAST_SCREEN, mGalleryPresenter.GRID_LAST);
    }

    @Test
    public void onSaveInstanceStateListTest() {
        when(mockGalleryMvpView.pagerOpen()).thenReturn(false);
        when(mockGalleryMvpView.gridType()).thenReturn(false);
        mGalleryPresenter.onSaveInstanceState(mockBundle);
        verify(mockBundle).putInt(mGalleryPresenter.LAST_SCREEN, mGalleryPresenter.LIST_LAST);
    }

    @Test
    public void restorePagerTest() {
        mGalleryPresenter.savedInstanceState = mockBundle;
        when(mockBundle.getInt(mGalleryPresenter.LAST_SCREEN)).thenReturn(mGalleryPresenter.PAGER_LAST);
        when(mockBundle.getInt(mGalleryPresenter.LAST_PAGER_POSITION)).thenReturn(100);
        mGalleryPresenter.restoreState();
        verify(mockGalleryMvpView).openPagerOnPosition(100);
    }

    @Test
    public void restoreGridTest() {
        mGalleryPresenter.savedInstanceState = mockBundle;
        when(mockBundle.getInt(mGalleryPresenter.LAST_SCREEN)).thenReturn(mGalleryPresenter.GRID_LAST);
        mGalleryPresenter.restoreState();
        verify(mockGalleryMvpView).toGrid();
    }

    @Test
    public void restoreListTest() {
        mGalleryPresenter.savedInstanceState = mockBundle;
        when(mockBundle.getInt(mGalleryPresenter.LAST_SCREEN)).thenReturn(mGalleryPresenter.LIST_LAST);
        mGalleryPresenter.restoreState();
        verify(mockGalleryMvpView).toList();
    }

    @Test
    public void onCreateOptionsMenuWithoutSavedStateTest() {
        mGalleryPresenter.savedInstanceState = null;
        mGalleryPresenter.onCreateOptionsMenu();
        verify(mockGalleryMvpView).showRecyclerMenu();
    }

    @Test
    public void onCreateOptionsMenuLastPagerTest() {
        mGalleryPresenter.savedInstanceState = mockBundle;
        when(mockBundle.getInt(mGalleryPresenter.LAST_SCREEN)).thenReturn(mGalleryPresenter.PAGER_LAST);
        mGalleryPresenter.onCreateOptionsMenu();
        verify(mockGalleryMvpView).showPagerMenu();
    }

    @Test
    public void onCreateOptionsMenuLastGridTest() {
        mGalleryPresenter.savedInstanceState = mockBundle;
        when(mockBundle.getInt(mGalleryPresenter.LAST_SCREEN)).thenReturn(mGalleryPresenter.GRID_LAST);
        mGalleryPresenter.onCreateOptionsMenu();
        verify(mockGalleryMvpView).showRecyclerMenu();
    }

    @Test
    public void onCreateOptionsMenuLastListTest() {
        mGalleryPresenter.savedInstanceState = mockBundle;
        when(mockBundle.getInt(mGalleryPresenter.LAST_SCREEN)).thenReturn(mGalleryPresenter.LIST_LAST);
        mGalleryPresenter.onCreateOptionsMenu();
        verify(mockGalleryMvpView).showRecyclerMenu();
    }
}
