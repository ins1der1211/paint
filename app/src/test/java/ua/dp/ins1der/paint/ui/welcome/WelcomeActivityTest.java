package ua.dp.ins1der.paint.ui.welcome;

import android.content.Intent;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.dp.ins1der.paint.BaseRobolectricTest;
import ua.dp.ins1der.paint.R;
import ua.dp.ins1der.paint.ui.register.RegisterActivity;
import ua.dp.ins1der.paint.ui.signin.SignInActivity;

import com.google.common.truth.Truth;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.robolectric.Robolectric;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.util.ActivityController;

import static com.google.common.truth.Truth.assertThat;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.robolectric.Shadows.shadowOf;

/**
 * Created by Yurchenko on 10.11.2016.
 */

public class WelcomeActivityTest extends BaseRobolectricTest {

    @Mock WelcomePresenter mockWelcomePresenter;

    WelcomeActivity welcomeActivity;

    @BindView(R.id.wa_b_sign_in) Button signInB;
    @BindView(R.id.wa_b_register) Button registerB;

    @Before
    public void setUp() {
        welcomeActivity = Robolectric.setupActivity(WelcomeActivity.class);
        welcomeActivity.mWelcomePresenter = mockWelcomePresenter;
        ButterKnife.bind(this, welcomeActivity);
    }

    @Test
    public void testStartSignInActivity() {
        signInB.performClick();
        Intent expected = new Intent(welcomeActivity, SignInActivity.class);
        assertThat(shadowOf(welcomeActivity).getNextStartedActivity().toString()).isEqualTo(expected.toString());
    }

    @Test
    public void testStartRegisterActivity() {
        registerB.performClick();
        Intent expected = new Intent(welcomeActivity, RegisterActivity.class);
        assertThat(shadowOf(welcomeActivity).getNextStartedActivity().toString()).isEqualTo(expected.toString());
    }

    @Test
    public void getStartIntentTest() {
        Intent expected = new Intent(RuntimeEnvironment.application, WelcomeActivity.class);
        assertTrue(expected.toString().equals(WelcomeActivity.getStartIntent(RuntimeEnvironment.application).toString()));
    }

    @Test
    public void onDestroyTest() {
        welcomeActivity.onDestroy();
        verify(mockWelcomePresenter).detachView();
    }

}
