package ua.dp.ins1der.paint.ui.gallery;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.robolectric.Robolectric;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.util.ActivityController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.dp.ins1der.paint.BaseRobolectricTest;
import ua.dp.ins1der.paint.R;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.robolectric.Shadows.shadowOf;

/**
 * Created by Yurchenko on 11.12.2016.
 */

public class GalleryActivityTest extends BaseRobolectricTest {

    @Mock GalleryPresenter mockGalleryPresenter;
    @Mock PopupMenu mockPopupMenu;

    @BindView(R.id.pictures_rv) RecyclerView picturesRV;
    @BindView(R.id.pictures_vp) ViewPager picturesVP;

    List<Bitmap> bitmapList;

    GalleryActivity galleryActivity;

    @Before
    public void setUp() throws IOException {
        super.setUp();
        galleryActivity = Robolectric.setupActivity(GalleryActivity.class);
        galleryActivity.mGalleryPresenter = mockGalleryPresenter;
        ButterKnife.bind(this, galleryActivity);
        bitmapList = new ArrayList<>();
        bitmapList.add(BitmapFactory.decodeFile(mTestUtils.readFile("test.png").getAbsolutePath()));
        bitmapList.add(BitmapFactory.decodeFile(mTestUtils.readFile("test.png").getAbsolutePath()));
        bitmapList.add(BitmapFactory.decodeFile(mTestUtils.readFile("test.png").getAbsolutePath()));
    }

    @Test
    public void getStartIntentTest() {
        String expected = new Intent(RuntimeEnvironment.application, GalleryActivity.class).toString();
        assertEquals(expected, galleryActivity.getStartIntent(RuntimeEnvironment.application).toString());
    }

    @Test
    public void visibilityTest() {
        assertEquals(View.VISIBLE, picturesRV.getVisibility());
        assertEquals(View.INVISIBLE, picturesVP.getVisibility());
        assertTrue(galleryActivity.menu.findItem(R.id.menu_item_grid).isVisible());
        assertTrue(galleryActivity.menu.findItem(R.id.menu_item_list).isVisible());
        assertFalse(galleryActivity.menu.findItem(R.id.menu_item_delete).isVisible());
    }

    @Test
    public void onItemClickTest() {
        galleryActivity.initRecyclerView(bitmapList);
        galleryActivity.initViewPager(bitmapList);
        galleryActivity.onItemClick(1);
        assertEquals(View.INVISIBLE, picturesRV.getVisibility());
        assertEquals(View.VISIBLE, picturesVP.getVisibility());
        assertEquals(picturesVP.getCurrentItem(), 1);
        assertFalse(galleryActivity.menu.findItem(R.id.menu_item_grid).isVisible());
        assertFalse(galleryActivity.menu.findItem(R.id.menu_item_list).isVisible());
        assertTrue(galleryActivity.menu.findItem(R.id.menu_item_delete).isVisible());
    }

    @Test
    public void onBackPressedTest() {
        galleryActivity.initRecyclerView(bitmapList);
        galleryActivity.initViewPager(bitmapList);
        galleryActivity.onItemClick(1);

        galleryActivity.onBackPressed();
        assertEquals(View.VISIBLE, picturesRV.getVisibility());
        assertEquals(View.INVISIBLE, picturesVP.getVisibility());
        assertTrue(galleryActivity.menu.findItem(R.id.menu_item_grid).isVisible());
        assertTrue(galleryActivity.menu.findItem(R.id.menu_item_list).isVisible());
        assertFalse(galleryActivity.menu.findItem(R.id.menu_item_delete).isVisible());

        ShadowActivity shadowActivity = shadowOf(galleryActivity);
        galleryActivity.onBackPressed();
        assertTrue(shadowActivity.isFinishing());
    }

    @Test
    public void onLongItemClickTest() {
        galleryActivity.popupMenu = mockPopupMenu;
        galleryActivity.onLongItemClick(3);
        assertEquals(3, galleryActivity.selectedItem);
        verify(mockPopupMenu).show();
    }

    @Test
    public void deleteFromPopupTest() {
        galleryActivity.onLongItemClick(3);
        galleryActivity.onMenuItemClick(galleryActivity.popupMenu.getMenu().findItem(R.id.popup_menu_delete));
        verify(mockGalleryPresenter).deleteImage(3);
    }

    @Test
    public void toGridTest() {
        galleryActivity.onOptionsItemSelected(galleryActivity.menu.findItem(R.id.menu_item_grid));
        assertTrue(picturesRV.getLayoutManager() instanceof GridLayoutManager);
    }

    @Test
    public void toListTest() {
        galleryActivity.onOptionsItemSelected(galleryActivity.menu.findItem(R.id.menu_item_list));
        assertTrue(picturesRV.getLayoutManager() instanceof LinearLayoutManager);
    }

    @Test
    public void recreateTest() {
        galleryActivity.recreate();
        verify(mockGalleryPresenter).onSaveInstanceState(any(Bundle.class));
        verify(mockGalleryPresenter).detachView();
    }

    @Test
    public void onRestoreInstanceStateTest() {
        galleryActivity = Robolectric.buildActivity(GalleryActivity.class).create().get();
        galleryActivity.mGalleryPresenter = mockGalleryPresenter;
        galleryActivity.onRestoreInstanceState(any(Bundle.class));
        verify(mockGalleryPresenter).onRestoreInstanceState(any(Bundle.class));
    }

    @Test
    public void onDestroyTest() {
        galleryActivity.onDestroy();
        verify(mockGalleryPresenter).detachView();
    }

    @Test
    public void onViewPagerDeleteTest() {
        galleryActivity.initRecyclerView(bitmapList);
        galleryActivity.initViewPager(bitmapList);
        galleryActivity.onItemClick(1);
        galleryActivity.onOptionsItemSelected(galleryActivity.menu.findItem(R.id.menu_item_delete));
        verify(mockGalleryPresenter).deleteImage(1);
    }
}
