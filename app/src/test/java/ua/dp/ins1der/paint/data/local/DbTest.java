package ua.dp.ins1der.paint.data.local;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.junit.Before;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import java.io.File;
import java.io.IOException;

import ua.dp.ins1der.paint.BaseRobolectricTest;
import ua.dp.ins1der.paint.R;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * Created by Yurchenko on 22.11.2016.
 */

public class DbTest extends BaseRobolectricTest {

    File file;

    @Before
    public void setUp() throws IOException {
        super.setUp();
        file = mTestUtils.readFile("test.png");
    }
}
