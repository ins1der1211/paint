package ua.dp.ins1der.paint;

import java.util.UUID;

/**
 * Created by Yurchenko on 17.11.2016.
 */

public class MockModelFabric {

    public static String randomString() {
        return UUID.randomUUID().toString();
    }
}
