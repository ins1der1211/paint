package ua.dp.ins1der.paint.data.local;

import android.content.Context;
import android.content.SharedPreferences;

import org.junit.Before;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import ua.dp.ins1der.paint.BaseRobolectricTest;
import ua.dp.ins1der.paint.data.local.model.User;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * Created by Yurchenko on 18.11.2016.
 */

public class PreferencesHelperTest extends BaseRobolectricTest {

    PreferencesHelper preferencesHelper;
    User user;

    @Before
    public void setUp() {
        preferencesHelper = new PreferencesHelper(RuntimeEnvironment.application);
        user = new User("token", 15);
    }

    @Test
    public void saveUserTest() {
        preferencesHelper.saveUser(user);
        assertTrue("token".equals(preferencesHelper.getToken()));
        assertTrue(15 == preferencesHelper.getUserId());
        assertTrue(preferencesHelper.isUserLoggedIn());
    }

    @Test
    public void deleteUserTest() {
        preferencesHelper.deleteUser();
        assertTrue("".equals(preferencesHelper.getToken()));
        assertTrue(-1 == preferencesHelper.getUserId());
        assertFalse(preferencesHelper.isUserLoggedIn());
    }

    @Test
    public void clearTest() {
        preferencesHelper.clear();
        assertFalse(preferencesHelper.isUserLoggedIn());
    }

}
