package ua.dp.ins1der.paint.ui.main;

import android.content.Intent;
import android.support.design.widget.NavigationView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.robolectric.Robolectric;
import org.robolectric.RuntimeEnvironment;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.dp.ins1der.paint.BaseRobolectricTest;
import ua.dp.ins1der.paint.R;
import ua.dp.ins1der.paint.ui.gallery.GalleryActivity;
import ua.dp.ins1der.paint.ui.paint.PaintActivity;
import ua.dp.ins1der.paint.ui.welcome.WelcomeActivity;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.robolectric.Shadows.shadowOf;

/**
 * Created by Yurchenko on 14.12.2016.
 */

public class MainActivityTest extends BaseRobolectricTest {

    private MainActivity mMainActivity;

    @Mock MainPresenter mockMainPresenter;

    @BindView(R.id.nav_view) NavigationView navigationView;

    @Before
    public void setUp() throws IOException {
        super.setUp();
        mMainActivity = Robolectric.setupActivity(MainActivity.class);
        ButterKnife.bind(this, mMainActivity);
    }

    @Test
    public void menuStartPaintTest() {
        mMainActivity.onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_paint));
        Intent expected = new Intent(mMainActivity, PaintActivity.class);
        assertEquals(expected.toString(), shadowOf(mMainActivity).getNextStartedActivity().toString());
    }

    @Test
    public void menuGalleryStartTest() {
        mMainActivity.onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_gallery));
        Intent expected = new Intent(mMainActivity, GalleryActivity.class);
        assertEquals(expected.toString(), shadowOf(mMainActivity).getNextStartedActivity().toString());
    }

    @Test
    public void menuLogoutTest() {
        mMainActivity.mainPresenter = mockMainPresenter;
        mMainActivity.onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_log_out));
        verify(mockMainPresenter).logOut();
        Intent expected = new Intent(mMainActivity, WelcomeActivity.class);
        assertEquals(expected.toString(), shadowOf(mMainActivity).getNextStartedActivity().toString());
        assertTrue(shadowOf(mMainActivity).isFinishing());
    }

    @Test
    public void cardStartPaintTest() {
        mMainActivity.onPaintClick();
        Intent expected = new Intent(mMainActivity, PaintActivity.class);
        assertEquals(expected.toString(), shadowOf(mMainActivity).getNextStartedActivity().toString());
    }

    @Test
    public void cardStartGalleryTest() {
        mMainActivity.onGalleryClick();
        Intent expected = new Intent(mMainActivity, GalleryActivity.class);
        assertEquals(expected.toString(), shadowOf(mMainActivity).getNextStartedActivity().toString());
    }

    @Test
    public void cardLogoutTest() {
        mMainActivity.mainPresenter = mockMainPresenter;
        mMainActivity.onLogoutClick();
        verify(mockMainPresenter).logOut();
        Intent expected = new Intent(mMainActivity, WelcomeActivity.class);
        assertEquals(expected.toString(), shadowOf(mMainActivity).getNextStartedActivity().toString());
        assertTrue(shadowOf(mMainActivity).isFinishing());
    }

    @Test
    public void getStartIntentTest() {
        Intent expected = new Intent(RuntimeEnvironment.application, MainActivity.class);
        assertTrue(expected.toString().equals(MainActivity.getStartIntent(RuntimeEnvironment.application).toString()));
    }
}
