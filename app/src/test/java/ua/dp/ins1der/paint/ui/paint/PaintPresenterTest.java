package ua.dp.ins1der.paint.ui.paint;

import android.graphics.Bitmap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import ua.dp.ins1der.paint.BaseTest;
import ua.dp.ins1der.paint.data.DataManager;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * Created by Yurchenko on 22.11.2016.
 */

@RunWith(MockitoJUnitRunner.class)
public class PaintPresenterTest extends BaseTest {

    @Mock PaintMvpView mockPaintMvpView;

    @Mock DataManager mockDataManager;

    @Mock Bitmap mockBitmap;

    PaintPresenter paintPresenter;

    @Before
    public void setUp() {
        paintPresenter = new PaintPresenter(mockDataManager);
        paintPresenter.attachView(mockPaintMvpView);
    }

    @Test
    public void saveBitmapTest() {
        paintPresenter.savePaint(mockBitmap);
        verify(mockDataManager).saveBitmap(mockBitmap);
        verifyNoMoreInteractions(mockDataManager);
    }


}
