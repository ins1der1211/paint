package ua.dp.ins1der.paint.ui.widgets.parcels;

import android.graphics.Paint;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Yurchenko on 08.11.2016.
 */

public class ParcelablePaint extends Paint implements Parcelable {
    private int color;
    private float strokeWidth;

    @Override
    public void setColor(int color) {
        this.color = color;
        super.setColor(color);
    }

    @Override
    public void setStrokeWidth(float width) {
        this.strokeWidth = width;
        super.setStrokeWidth(width);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.color);
        dest.writeFloat(this.strokeWidth);
    }

    public ParcelablePaint() {
    }

    protected ParcelablePaint(Parcel in) {
        this.color = in.readInt();
        this.strokeWidth = in.readFloat();
    }

    public static final Parcelable.Creator<ParcelablePaint> CREATOR = new Parcelable.Creator<ParcelablePaint>() {
        @Override
        public ParcelablePaint createFromParcel(Parcel source) {
            return new ParcelablePaint(source);
        }

        @Override
        public ParcelablePaint[] newArray(int size) {
            return new ParcelablePaint[size];
        }
    };
}
