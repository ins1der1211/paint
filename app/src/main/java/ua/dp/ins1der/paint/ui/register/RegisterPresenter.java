package ua.dp.ins1der.paint.ui.register;

import ua.dp.ins1der.paint.data.DataManager;
import ua.dp.ins1der.paint.data.remote.model.RegisterRequest;
import ua.dp.ins1der.paint.ui.base.Presenter;
import ua.dp.ins1der.paint.util.EncodingUtil;
import ua.dp.ins1der.paint.util.TextUtils;

import javax.inject.Inject;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by Yurchenko on 28.10.2016.
 */

public class RegisterPresenter implements Presenter<RegisterMvpView> {
    private RegisterMvpView mRegisterMvpView;

    private final DataManager mDataManager;
    private Subscription mSubscription;

    @Inject
    public RegisterPresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    @Override
    public void attachView(RegisterMvpView mvpView) {
        mRegisterMvpView = mvpView;
    }

    @Override
    public void detachView() {
        mRegisterMvpView = null;
        if(mSubscription != null) {
            mSubscription.unsubscribe();
        }
    }

    public void onRegisterClick() {
        mRegisterMvpView.showProgress();

        String email = mRegisterMvpView.getEmail();
        String password = EncodingUtil.toMD5(mRegisterMvpView.getPassword());
        String passwordRepeat = EncodingUtil.toMD5(mRegisterMvpView.getPasswordRepeat());

        if(TextUtils.isEmpty(email) || !password.equals(passwordRepeat)) {
            mRegisterMvpView.showPasswordsNotMatch();
            mRegisterMvpView.hideProgress();
            return;
        }

        mSubscription = mDataManager.register(new RegisterRequest(email, password, passwordRepeat))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        registerResponse -> {
                            mRegisterMvpView.hideProgress();
                            mRegisterMvpView.startMainActivity();
                        },
                        (e) -> {
                            if(e instanceof HttpException) {
                                Timber.e("response code " + ((HttpException) e).response().code());
                            }
                            mRegisterMvpView.showRegisterFailed(e.getMessage());
                            mRegisterMvpView.hideProgress();
                        }
                );
    }
}
