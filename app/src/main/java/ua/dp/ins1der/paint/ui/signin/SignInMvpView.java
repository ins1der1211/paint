package ua.dp.ins1der.paint.ui.signin;

import android.support.annotation.StringRes;

import ua.dp.ins1der.paint.ui.base.MvpView;

/**
 * Created by Yurchenko on 28.10.2016.
 */

public interface SignInMvpView extends MvpView {

    String getEmail();

    String getPassword();

    void onSignInClick();

    void onGoogleSignClick();

    void signinFail(@StringRes int errorMessage);

    void startMainActivity();
}
