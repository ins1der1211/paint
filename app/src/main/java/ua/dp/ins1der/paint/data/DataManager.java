package ua.dp.ins1der.paint.data;

import android.graphics.Bitmap;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;
import ua.dp.ins1der.paint.data.local.DatabaseHelper;
import ua.dp.ins1der.paint.data.local.FileHelper;
import ua.dp.ins1der.paint.data.local.PreferencesHelper;
import ua.dp.ins1der.paint.data.local.mappers.FileToImageMapper;
import ua.dp.ins1der.paint.data.local.model.Image;
import ua.dp.ins1der.paint.data.remote.PaintService;
import ua.dp.ins1der.paint.data.remote.model.RegisterRequest;
import ua.dp.ins1der.paint.data.remote.model.RegisterResponse;
import ua.dp.ins1der.paint.data.remote.model.SignInRequest;
import ua.dp.ins1der.paint.data.remote.model.SignInResponse;
import ua.dp.ins1der.paint.data.remote.model.UploadResponse;
import ua.dp.ins1der.paint.util.EventPosterHelper;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;

/**
 * Created by Yurchenko on 26.10.2016.
 */
@Singleton
public class DataManager {
    private final PaintService mPaintService;
    private final DatabaseHelper mDatabaseHelper;
    private final PreferencesHelper mPreferencesHelper;
    private final EventPosterHelper mEventPosterHelper;
    private final FileHelper mFileHelper;

    @Inject
    public DataManager(PaintService paintService,
                       DatabaseHelper databaseHelper,
                       PreferencesHelper preferencesHelper,
                       EventPosterHelper eventPosterHelper,
                       FileHelper fileHelper) {
        mPaintService = paintService;
        mDatabaseHelper = databaseHelper;
        mPreferencesHelper = preferencesHelper;
        mEventPosterHelper = eventPosterHelper;
        mFileHelper = fileHelper;
    }

    public Observable<RegisterResponse> register(RegisterRequest registerRequest) {
        return mPaintService.register(registerRequest)
                .doOnNext(registerResponse -> mPreferencesHelper.saveUser(registerResponse.getUser()));
    }

    public Observable<SignInResponse> signin(SignInRequest signInRequest) {
        return mPaintService.signin(signInRequest)
                .doOnNext(signInResponse -> mPreferencesHelper.saveUser(signInResponse.getUser()));
    }

    public void logOut() {
        mPreferencesHelper.deleteUser();
    }

    public boolean isUserLoggedIn() {
        return mPreferencesHelper.isUserLoggedIn();
    }

    public void saveBitmap(Bitmap b) {
        mFileHelper.saveBitmap(b)
                .map(new FileToImageMapper())
                .subscribe(
                        image -> {
                            mDatabaseHelper.saveBitmap(image)
                                    .observeOn(Schedulers.io())
                                    .subscribeOn(AndroidSchedulers.mainThread())
                                    .subscribe(
                                            result -> Timber.d("Inserted: " + result),
                                            e -> Timber.e(e.getMessage())
                                    );
                        },
                        e -> {});
    }

    public Observable<Boolean> deleteFromFiles(Image image) {
        mDatabaseHelper.markAsDeleted(image).subscribe();
        return mFileHelper.deleteFile(new File(image.getImagePath()));
    }

    public void upload(Image image) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), new File(image.getImagePath()));
        MultipartBody.Part part = MultipartBody.Part.createFormData("picture", image.getImagePath(), requestBody);
        mPaintService.upload(part)
                .map(UploadResponse::getServerId)
                .subscribe(
                        serverId -> {
                            image.setServerId(serverId);
                            mDatabaseHelper.markAsSync(image).subscribe();
                        },
                        e -> Timber.e(e.getMessage())
                );
    }

    public Observable<List<Image>> getImagesForGallery() {
        return mDatabaseHelper.getImageList()
                .toList();
    }

    public Observable<Image> getNotSyncImages() {
        return mDatabaseHelper.getNotSyncImages();
    }

    public void deleteFromDb(Image image) {
        mPaintService.delete(image.getServerId())
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        success -> {
                            mDatabaseHelper.delete(image).subscribe(
                                    number -> Timber.d("Delete from db: " + number + "rows"),
                                    e -> Timber.e(e.getMessage())
                            );
                        },
                        e -> Timber.e(e.getMessage())
                );
    }
}
