package ua.dp.ins1der.paint.ui.paint;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import ua.dp.ins1der.paint.R;
import ua.dp.ins1der.paint.ui.base.BaseActivity;
import ua.dp.ins1der.paint.ui.widgets.DrawingView;
import ua.dp.ins1der.paint.ui.widgets.parcels.Layer;
import ua.dp.ins1der.paint.util.DialogFactory;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Yurchenko on 27.10.2016.
 */

public class PaintActivity extends BaseActivity implements PaintMvpView,
        ColorPickerDialog.OnColorSelectedListener, ThiknessDialog.OnSeekListener {

    @Inject PaintPresenter mPaintPresenter;

    @BindView(R.id.pa_drap) DrawingView drawingView;
    @BindView(R.id.fab_choose_color) FloatingActionButton chooseColorFab;
    @BindView(R.id.fab_erase) FloatingActionButton eraseFab;
    @BindView(R.id.fab_thikness) FloatingActionButton thiknessFab;
    @BindView(R.id.fab_rollback) FloatingActionButton rollBackFab;
    @BindView(R.id.fab_scale) FloatingActionButton scaleFab;
    @BindView(R.id.pa_fab_open_menu) FloatingActionButton openMenuFab;

    private Animation openMenuAnim;
    private Animation closeMenuAnim;

    private boolean menuOpened = false;

    private int lastColor = Color.BLACK;

    private float lastThickness = 1f;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, PaintActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paint);
        ButterKnife.bind(this);
        activityComponent().inject(this);
        mPaintPresenter.attachView(this);
        loadAnimations();
        setHideFabListener();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        mPaintPresenter.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        mPaintPresenter.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        DialogFactory.createSimpleOkErrorDialog(this,
                R.string.attention,
                R.string.save_paint,
                (dialog, ok) -> {
                    drawingView.buildDrawingCache();
                    mPaintPresenter.savePaint(drawingView.getDrawingCache());
                    drawingView.destroyDrawingCache();
                    finish();
                },
                (dialog, cancel) -> finish())
                .show();
    }

    @Override
    protected void onDestroy() {
        mPaintPresenter.detachView();
        super.onDestroy();
    }

    private void loadAnimations() {
        openMenuAnim = AnimationUtils.loadAnimation(this, R.anim.show_fab);
        closeMenuAnim = AnimationUtils.loadAnimation(this, R.anim.hide_fab);
    }

    @OnClick(R.id.pa_fab_open_menu)
    public void toggleFabMenu() {
        if(!menuOpened) openFabMenu();
        else closeFabMenu();
    }

    @OnClick(R.id.fab_choose_color)
    public void openChooseColorDialog() {
        new ColorPickerDialog(this, lastColor, this).show();
    }

    @OnClick(R.id.fab_thikness)
    public void openThiknessDialog() {
        new ThiknessDialog(this, (int)lastThickness, this).show();
    }

    @OnClick(R.id.fab_erase)
    public void setEraser() {
        drawingView.setEraser();
    }

    @OnClick(R.id.fab_scale)
    public void setDefaultScale() {
        drawingView.setDefaultScale();
    }

    @OnClick(R.id.fab_rollback)
    public void rollBack() {
        drawingView.rollBack();
    }

    private void openFabMenu() {
        openMenuFab.animate()
                .withStartAction(() -> openMenuFab.animate().setDuration(400).rotation(90f).start())
                .withEndAction(() -> openMenuFab.setRotation(90f))
                .start();
        chooseColorFab.startAnimation(openMenuAnim);
        eraseFab.startAnimation(openMenuAnim);
        thiknessFab.startAnimation(openMenuAnim);
        scaleFab.startAnimation(openMenuAnim);
        rollBackFab.startAnimation(openMenuAnim);
        chooseColorFab.setClickable(true);
        eraseFab.setClickable(true);
        thiknessFab.setClickable(true);
        scaleFab.setClickable(true);
        rollBackFab.setClickable(true);
        menuOpened = !menuOpened;
    }

    private void closeFabMenu() {
        openMenuFab.animate()
                .withStartAction(() -> openMenuFab.animate().setDuration(400).rotation(0f).start())
                .withEndAction(() -> openMenuFab.setRotation(0f))
                .start();
        chooseColorFab.startAnimation(closeMenuAnim);
        eraseFab.startAnimation(closeMenuAnim);
        thiknessFab.startAnimation(closeMenuAnim);
        scaleFab.startAnimation(closeMenuAnim);
        rollBackFab.startAnimation(closeMenuAnim);
        chooseColorFab.setClickable(false);
        eraseFab.setClickable(false);
        thiknessFab.setClickable(false);
        scaleFab.setClickable(false);
        rollBackFab.setClickable(false);
        menuOpened = !menuOpened;
    }

    private void hideFabMenu() {
        openMenuFab.animate()
                .withStartAction(() -> {
                    openMenuFab.animate().setDuration(400).alpha(0).start();
                    if(menuOpened)
                        closeFabMenu();
                })
                .withEndAction(() -> openMenuFab.setVisibility(View.GONE))
                .start();
    }

    private void showFabMenu() {
        openMenuFab.animate()
                .withStartAction(() -> openMenuFab.setVisibility(View.VISIBLE))
                .withEndAction(() -> openMenuFab.animate().setDuration(400).alpha(1).start())
                .start();
    }

    @Override
    public void onColorSelected(int color) {
        lastColor = color;
        drawingView.setColor(color);
    }

    @Override
    public void onThicknessSelected(float thickness) {
        lastThickness = thickness;
        drawingView.setThickness(thickness);
    }

    private void setHideFabListener() {
        drawingView.setDoubleClickListener(() -> {
            if(openMenuFab.getVisibility() == View.VISIBLE) {
                hideFabMenu();
            } else {
                showFabMenu();
            }
        });
    }

    @Override
    public int getLastColor() {
        return lastColor;
    }

    @Override
    public float getLastThickness() {
        return lastThickness;
    }

    @Override
    public List<Layer> getLayerList() {
        return drawingView.getLayerList();
    }

    @Override
    public List<Bitmap> getBitmapList() {
        return drawingView.getBitmapList();
    }

    @Override
    public void setLastColor(int color) {
        lastColor = color;
    }

    @Override
    public void setLastThickness(float lastThickness) {
        this.lastThickness = lastThickness;
    }

    @Override
    public void setLayerList(List<Layer> layerList) {
        drawingView.setLayerList(layerList);
    }

    @Override
    public void setBitmapList(List<Bitmap> bitmapList) {
        drawingView.setBitmapList(bitmapList);
    }
}
