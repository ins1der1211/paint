package ua.dp.ins1der.paint.data.remote.model;

import com.google.gson.annotations.SerializedName;

import ua.dp.ins1der.paint.data.local.model.User;

/**
 * Created by Yurchenko on 28.10.2016.
 */

public class SignInResponse extends BaseResponse {

    @SerializedName("user")
    private User user;

    public User getUser() {
        return user;
    }
}