package ua.dp.ins1der.paint.data.local;

import android.content.Context;
import android.content.SharedPreferences;

import ua.dp.ins1der.paint.data.local.model.Image;
import ua.dp.ins1der.paint.data.local.model.User;
import ua.dp.ins1der.paint.injection.ApplicationContext;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Yurchenko on 26.10.2016.
 */

@Singleton
public class PreferencesHelper {
    public static final String PREF_FILE_NAME = "paint_app_pref_file";

    public static final String USER = "user";

    private final SharedPreferences mPref;
    private final Gson mGson;

    @Inject
    public PreferencesHelper(@ApplicationContext Context context) {
        mPref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        mGson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    }

    public void clear() {
        mPref.edit().clear().apply();
    }

    public void saveUser(User user) {
        mPref.edit().putString(USER, mGson.toJson(user)).apply();
    }

    public String getToken() {
        if(isUserLoggedIn()) return mGson.fromJson(mPref.getString(USER, ""), User.class).getToken();
        else return "";
    }

    public int getUserId() {
        if(isUserLoggedIn()) return mGson.fromJson(mPref.getString(USER, ""), User.class).getUserId();
        else return -1;
    }

    public void deleteUser() {
        mPref.edit().remove(USER).apply();
    }

    public boolean isUserLoggedIn() {
        return !mPref.getString(USER, "").isEmpty();
    }
}
