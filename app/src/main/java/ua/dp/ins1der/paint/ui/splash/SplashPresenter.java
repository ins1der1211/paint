package ua.dp.ins1der.paint.ui.splash;

import ua.dp.ins1der.paint.data.DataManager;
import ua.dp.ins1der.paint.ui.base.Presenter;

import javax.inject.Inject;

/**
 * Created by Yurchenko on 17.11.2016.
 */

public class SplashPresenter implements Presenter<SplashMvpView> {
    private SplashMvpView mSplashMvpView;

    private final DataManager mDataManager;

    @Inject
    public SplashPresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    @Override
    public void attachView(SplashMvpView mvpView) {
        mSplashMvpView = mvpView;
    }

    @Override
    public void detachView() {
        mSplashMvpView = null;
    }

    public boolean isUserLoggedIn() {
        return mDataManager.isUserLoggedIn();
    }
}
