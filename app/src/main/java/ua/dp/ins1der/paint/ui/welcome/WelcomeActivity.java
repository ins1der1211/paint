package ua.dp.ins1der.paint.ui.welcome;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import ua.dp.ins1der.paint.R;
import ua.dp.ins1der.paint.ui.base.BaseActivity;
import ua.dp.ins1der.paint.ui.register.RegisterActivity;
import ua.dp.ins1der.paint.ui.signin.SignInActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Yurchenko on 27.10.2016.
 */

public class WelcomeActivity extends BaseActivity implements WelcomeMvpView {

    @Inject WelcomePresenter mWelcomePresenter;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, WelcomeActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        activityComponent().inject(this);
        mWelcomePresenter.attachView(this);
        ButterKnife.bind(this);
    }

    @Override
    protected void onDestroy() {
        mWelcomePresenter.detachView();
        super.onDestroy();
    }

    @OnClick(R.id.wa_b_sign_in)
    @Override
    public void onSignInClick() {
        startActivity(SignInActivity.getStartIntent(this));
    }

    @OnClick(R.id.wa_b_register)
    @Override
    public void onRegisterClick() {
        startActivity(RegisterActivity.getStartIntent(this));
    }
}
