package ua.dp.ins1der.paint.ui.splash;

import android.os.Bundle;

import ua.dp.ins1der.paint.R;
import ua.dp.ins1der.paint.ui.base.BaseActivity;
import ua.dp.ins1der.paint.ui.main.MainActivity;
import ua.dp.ins1der.paint.ui.welcome.WelcomeActivity;
import ua.dp.ins1der.paint.util.FileUtils;

import javax.inject.Inject;

/**
 * Created by Yurchenko on 27.10.2016.
 */

public class SplashActivity extends BaseActivity {

    @Inject FileUtils mFileUtils;

    @Inject SplashPresenter mSplashPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        activityComponent().inject(this);
        mFileUtils.createAppDir();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHandler.postDelayed(() -> {
            if(mSplashPresenter.isUserLoggedIn())
                startActivity(MainActivity.getStartIntent(this));
            else
                startActivity(WelcomeActivity.getStartIntent(mContext));
            finish();
        }, 2000);
    }
}
