package ua.dp.ins1der.paint.util;

import android.os.Handler;
import android.os.Looper;

import com.squareup.otto.Bus;

import javax.inject.Inject;

/**
 * Created by Yurchenko on 26.10.2016.
 */

public class EventPosterHelper {
    private final Bus mBus;

    @Inject
    public EventPosterHelper(Bus bus) {
        mBus = bus;
    }

    public void postEventSafely(final Object event) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                mBus.post(event);
            }
        });
    }
}
