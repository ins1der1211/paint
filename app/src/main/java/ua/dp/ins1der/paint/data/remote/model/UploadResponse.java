package ua.dp.ins1der.paint.data.remote.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Yurchenko on 08.12.2016.
 */

public class UploadResponse {
    @SerializedName("server_id")
    private String serverId;

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }
}
