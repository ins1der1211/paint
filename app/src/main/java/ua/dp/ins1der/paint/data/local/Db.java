package ua.dp.ins1der.paint.data.local;

import android.content.ContentValues;
import android.database.Cursor;
import android.provider.BaseColumns;

import java.io.File;

import ua.dp.ins1der.paint.data.local.model.Image;

/**
 * Created by Yurchenko on 26.10.2016.
 */

public class Db {
    public Db() { }

    public static final class ImagesTable implements BaseColumns {
        public static final String TABLE_NAME = "images";

        public static final String COLUMN_IMAGE = "image";
        public static final String COLUMN_SYNC = "sync";
        public static final String COLUMN_DELETED = "deleted";
        public static final String COLUMN_SERVER_ID = "server_id";
        public static final String COLUMN_USER_ID = "user_id";

        public static final String CREATE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        COLUMN_IMAGE + " TEXT, " +
                        COLUMN_SYNC + " TEXT, " +
                        COLUMN_DELETED + " TEXT, " +
                        COLUMN_SERVER_ID + " TEXT, " +
                        COLUMN_USER_ID + " TEXT " +
                        " );";

        public static ContentValues toContentValues(Image image) {
            ContentValues values = new ContentValues();
            values.put(COLUMN_IMAGE, image.getImagePath());
            values.put(COLUMN_SYNC, String.valueOf(image.getSync()));
            values.put(COLUMN_DELETED, String.valueOf(image.getDeleted()));
            values.put(COLUMN_SERVER_ID, image.getServerId());
            return values;
        }

        public static Image populateContent(Cursor cursor) {
            Image image = new Image();
            image.setImagePath(cursor.getString(cursor.getColumnIndex(ImagesTable.COLUMN_IMAGE)));
            image.setSync(Integer.valueOf(cursor.getString(cursor.getColumnIndex(ImagesTable.COLUMN_SYNC))));
            image.setDeleted(Integer.valueOf(cursor.getString(cursor.getColumnIndex(ImagesTable.COLUMN_DELETED))));
            image.setServerId(cursor.getString(cursor.getColumnIndex(ImagesTable.COLUMN_SERVER_ID)));
            return image;
        }

    }
}
