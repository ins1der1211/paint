package ua.dp.ins1der.paint;

import android.app.Application;
import android.content.Context;

import ua.dp.ins1der.paint.injection.component.AppComponent;
import ua.dp.ins1der.paint.injection.component.DaggerAppComponent;
import ua.dp.ins1der.paint.injection.module.AppModule;

import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.Task;
import com.squareup.otto.Bus;

import javax.inject.Inject;

import timber.log.Timber;
import ua.dp.ins1der.paint.injection.module.SyncModule;

/**
 * Created by Yurchenko on 26.10.2016.
 */

public class PaintApp extends Application {
    @Inject Bus mEventBus;
    @Inject GcmNetworkManager mGcmNetworkManager;
    @Inject Task mSyncTask;
    AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        if(BuildConfig.DEBUG) Timber.plant(new Timber.DebugTree());

        mAppComponent = buildComponent();
        mAppComponent.inject(this);
        mEventBus.register(this);

        /* Robolectric can't schedule this task for an unknown reason, and fires IllegalArgumentException */
        try {mGcmNetworkManager.schedule(mSyncTask);}
        catch (IllegalArgumentException e) {}
    }

    public static PaintApp get(Context context) {
        return (PaintApp) context.getApplicationContext();
    }

    public AppComponent getComponent() {
        return mAppComponent;
    }

    public AppComponent buildComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .syncModule(new SyncModule(this))
                .build();
    }

}
