package ua.dp.ins1der.paint.injection.component;

import ua.dp.ins1der.paint.injection.PerActivity;
import ua.dp.ins1der.paint.injection.module.ActivityModule;
import ua.dp.ins1der.paint.ui.gallery.GalleryActivity;
import ua.dp.ins1der.paint.ui.main.MainActivity;
import ua.dp.ins1der.paint.ui.paint.PaintActivity;
import ua.dp.ins1der.paint.ui.register.RegisterActivity;
import ua.dp.ins1der.paint.ui.signin.SignInActivity;
import ua.dp.ins1der.paint.ui.splash.SplashActivity;
import ua.dp.ins1der.paint.ui.welcome.WelcomeActivity;

import dagger.Component;

/**
 * Created by Yurchenko on 26.10.2016.
 */
@PerActivity
@Component(
        dependencies = AppComponent.class,
        modules = {ActivityModule.class})

public interface ActivityComponent {

    void inject(SplashActivity activity);

    void inject(WelcomeActivity activity);

    void inject(SignInActivity activity);

    void inject(PaintActivity activity);

    void inject(RegisterActivity activity);

    void inject(MainActivity activity);

    void inject(GalleryActivity activity);
}
