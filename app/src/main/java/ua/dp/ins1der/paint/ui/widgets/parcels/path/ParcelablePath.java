package ua.dp.ins1der.paint.ui.widgets.parcels.path;

import android.graphics.Path;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Yurchenko on 08.11.2016.
 */

public class ParcelablePath extends Path implements Parcelable {
    private ArrayList<ParcelableQuad> quadList = new ArrayList<>();
    private ArrayList<ParcelableMove> moveList = new ArrayList<>();

    @Override
    public void moveTo(float x, float y) {
        moveList.add(new ParcelableMove(x, y));
        super.moveTo(x, y);
    }

    @Override
    public void quadTo(float x, float y, float x1, float y1){
        quadList.add(new ParcelableQuad(x, y, x1, y1));
        super.quadTo(x, y, x1, y1);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.quadList);
        dest.writeTypedList(this.moveList);
    }

    public ParcelablePath() {
    }

    protected ParcelablePath(Parcel in) {
        this.quadList = in.createTypedArrayList(ParcelableQuad.CREATOR);
        this.moveList = in.createTypedArrayList(ParcelableMove.CREATOR);
    }

    public static final Parcelable.Creator<ParcelablePath> CREATOR = new Parcelable.Creator<ParcelablePath>() {
        @Override
        public ParcelablePath createFromParcel(Parcel source) {
            return new ParcelablePath(source);
        }

        @Override
        public ParcelablePath[] newArray(int size) {
            return new ParcelablePath[size];
        }
    };

    public boolean isEmpty() {
        return (quadList.isEmpty() && moveList.isEmpty());
    }
}
