package ua.dp.ins1der.paint.ui.paint;

import android.graphics.Bitmap;

import java.util.List;

import ua.dp.ins1der.paint.ui.base.MvpView;
import ua.dp.ins1der.paint.ui.widgets.DrawingView;
import ua.dp.ins1der.paint.ui.widgets.parcels.Layer;

/**
 * Created by Yurchenko on 27.10.2016.
 */

public interface PaintMvpView extends MvpView {

    int getLastColor();

    float getLastThickness();

    List<Layer> getLayerList();

    List<Bitmap> getBitmapList();

    void setLastColor(int color);

    void setLastThickness(float lastThickness);

    void setLayerList(List<Layer> layerList);

    void setBitmapList(List<Bitmap> bitmapList);
}
