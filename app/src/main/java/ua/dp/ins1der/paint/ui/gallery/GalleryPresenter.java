package ua.dp.ins1der.paint.ui.gallery;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;
import ua.dp.ins1der.paint.data.DataManager;
import ua.dp.ins1der.paint.data.local.model.Image;
import ua.dp.ins1der.paint.ui.base.Presenter;

/**
 * Created by Yurchenko on 24.11.2016.
 */

public class GalleryPresenter implements Presenter<GalleryMvpView> {

    private GalleryMvpView mGalleryMvpView;

    final DataManager mDataManager;

    private Subscription mSubscription;

    private List<Image> mImageList;

    Bundle savedInstanceState;

    static final String LAST_SCREEN = "last_screen";
    static final String LAST_PAGER_POSITION = "last_pager_position";
    static final int GRID_LAST = 0x01;
    static final int LIST_LAST = 0x02;
    static final int PAGER_LAST = 0x03;

    @Inject
    public GalleryPresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    @Override
    public void attachView(GalleryMvpView mvpView) {
        mGalleryMvpView = mvpView;
    }

    @Override
    public void detachView() {
        if(mSubscription != null)
            mSubscription.unsubscribe();
        mGalleryMvpView = null;
    }

    public void getImagesForGallery() {
        mSubscription = mDataManager.getImagesForGallery()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(imageList -> mImageList = imageList)
                .flatMap(Observable::from)
                .map(image -> BitmapFactory.decodeFile(image.getImagePath()))
                .toList()
                .subscribe(
                        bitmapList -> {
                            mGalleryMvpView.initRecyclerView(bitmapList);
                            mGalleryMvpView.initViewPager(bitmapList);
                            if (savedInstanceState != null) restoreState();
                        },
                        e -> Timber.e(e.getMessage())
                );

    }

    public List<Image> getImageList() {
        return mImageList;
    }

    public void deleteImage(int position) {
        mSubscription = mDataManager.deleteFromFiles(mImageList.get(position))
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        value -> {
                            if(value) getImagesForGallery();
                            else Timber.e("File not deleted: " + mImageList.get(position).getImagePath());
                        },
                        e -> Timber.e("File not deleted: " + mImageList.get(position).getImagePath())
                );
    }

    public void onSaveInstanceState(Bundle outState) {
        if(mGalleryMvpView.pagerOpen()) {
            outState.putInt(LAST_SCREEN, PAGER_LAST);
            outState.putInt(LAST_PAGER_POSITION, mGalleryMvpView.pagerPosition());
        } else {
            if(mGalleryMvpView.gridType()) outState.putInt(LAST_SCREEN, GRID_LAST);
            else outState.putInt(LAST_SCREEN, LIST_LAST);
        }
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        this.savedInstanceState = savedInstanceState;
    }

    void restoreState() {
        if(savedInstanceState.getInt(LAST_SCREEN) == PAGER_LAST)
            mGalleryMvpView.openPagerOnPosition(savedInstanceState.getInt(LAST_PAGER_POSITION));
        else {
            if(savedInstanceState.getInt(LAST_SCREEN) == GRID_LAST) mGalleryMvpView.toGrid();
            else mGalleryMvpView.toList();
        }
    }

    public void onCreateOptionsMenu() {
        if(savedInstanceState != null &&
                savedInstanceState.getInt(LAST_SCREEN) == PAGER_LAST)
            mGalleryMvpView.showPagerMenu();
        else mGalleryMvpView.showRecyclerMenu();
    }
}
