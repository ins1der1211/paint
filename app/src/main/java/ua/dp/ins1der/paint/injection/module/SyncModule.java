package ua.dp.ins1der.paint.injection.module;

import android.content.Context;
import android.icu.util.TimeUnit;

import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.PeriodicTask;
import com.google.android.gms.gcm.Task;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rx.schedulers.TimeInterval;
import ua.dp.ins1der.paint.data.remote.SyncService;

/**
 * Created by Yurchenko on 08.12.2016.
 */

@Module
public class SyncModule {

    private Context mContext;

    public SyncModule(Context context) {
        mContext = context;
    }

    @Provides
    @Singleton
    GcmNetworkManager provideGcmNetworkManager() {
        return GcmNetworkManager.getInstance(mContext);
    }

    @Provides
    Task provideTask() {
        return new PeriodicTask.Builder()
                .setService(SyncService.class)
                .setPeriod(5)
                .setFlex(1)
                .setTag(SyncService.class.getSimpleName())
                .setPersisted(true)
                .build();
    }
}
