package ua.dp.ins1der.paint.ui.gallery;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.dp.ins1der.paint.R;
import ua.dp.ins1der.paint.ui.base.BaseActivity;

/**
 * Created by Yurchenko on 24.11.2016.
 */

public class GalleryActivity extends BaseActivity implements GalleryMvpView,
        PictureRecyclerViewAdapter.InteractionListrener, PopupMenu.OnMenuItemClickListener {

    @Inject GalleryPresenter mGalleryPresenter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.pictures_rv) RecyclerView picturesRV;
    @BindView(R.id.pictures_vp) ViewPager picturesVP;
    @BindView(R.id.root) CoordinatorLayout root;

    PictureRecyclerViewAdapter pictureRecyclerViewAdapter;
    PicturePagerAdapter picturePagerAdapter;
    Menu menu;
    PopupMenu popupMenu;

    int selectedItem;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, GalleryActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        activityComponent().inject(this);
        ButterKnife.bind(this);
        mGalleryPresenter.attachView(this);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        createPopupMenu();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        mGalleryPresenter.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGalleryPresenter.getImagesForGallery();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        mGalleryPresenter.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        mGalleryPresenter.detachView();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gallery_menu, menu);
        this.menu = menu;
        mGalleryPresenter.onCreateOptionsMenu();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(pagerOpen()) {
                    closePager();
                    showRecyclerMenu();
                }
                else onBackPressed();
                break;
            case R.id.menu_item_grid:
                toGrid();
                break;
            case R.id.menu_item_list:
                toList();
                break;
            case R.id.menu_item_delete:
                mGalleryPresenter.deleteImage(picturesVP.getCurrentItem());
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(pagerOpen()) {
            closePager();
            showRecyclerMenu();
        } else
            super.onBackPressed();
    }

    private void createPopupMenu() {
        popupMenu = new PopupMenu(this, picturesRV);
        popupMenu.inflate(R.menu.gallery_popup_menu);
        popupMenu.setOnMenuItemClickListener(this);
    }

    @Override
    public boolean pagerOpen() {
        return picturesVP.getVisibility() == View.VISIBLE;
    }

    @Override
    public boolean gridType() {
        return picturesRV.getLayoutManager() instanceof GridLayoutManager;
    }

    @Override
    public int pagerPosition() {
        return picturesVP.getCurrentItem();
    }

    private void closePager() {
        picturesRV.setVisibility(View.VISIBLE);
        picturesVP.setVisibility(View.INVISIBLE);
    }

    @Override
    public void initRecyclerView(List<Bitmap> bitmapList) {
        picturesRV.setLayoutManager(new GridLayoutManager(this, 2));
        pictureRecyclerViewAdapter = new PictureRecyclerViewAdapter(bitmapList);
        picturesRV.setAdapter(pictureRecyclerViewAdapter);
        pictureRecyclerViewAdapter.setOnItemClickListener(this);
    }

    @Override
    public void initViewPager(List<Bitmap> bitmapList) {
        picturePagerAdapter = new PicturePagerAdapter(bitmapList);
        picturesVP.setAdapter(picturePagerAdapter);
    }

    @Override
    public void onItemClick(int position) {
        picturesRV.setVisibility(View.INVISIBLE);
        picturesVP.setVisibility(View.VISIBLE);
        picturesVP.setCurrentItem(position);
        showPagerMenu();
    }

    @Override
    public void onLongItemClick(int position) {
        selectedItem = position;
        popupMenu.show();
    }

    @Override
    public void toGrid() {
        picturesRV.setLayoutManager(new GridLayoutManager(this, 2));
    }

    @Override
    public void toList() {
        picturesRV.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void openPagerOnPosition(int position) {
        onItemClick(position);
    }

    @Override
    public void showPagerMenu() {
        menu.setGroupVisible(R.id.recycler_menu, false);
        menu.setGroupVisible(R.id.pager_menu, true);
    }

    @Override
    public void showRecyclerMenu() {
        menu.setGroupVisible(R.id.recycler_menu, true);
        menu.setGroupVisible(R.id.pager_menu, false);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.popup_menu_delete:
                mGalleryPresenter.deleteImage(selectedItem);
                break;
        }
        return false;
    }
}
