package ua.dp.ins1der.paint.data.local.mappers;

import java.io.File;

import rx.functions.Func1;
import ua.dp.ins1der.paint.data.local.model.Image;

/**
 * Created by Yurchenko on 11.12.2016.
 */

public class FileToImageMapper implements Func1<File, Image> {
    @Override
    public Image call(File file) {
        Image image = new Image();
        image.setImagePath(file.getAbsolutePath());
        image.setDeleted(0);
        image.setSync(1);
        image.setServerId("0");
        return image;
    }
}
