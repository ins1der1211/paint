package ua.dp.ins1der.paint.ui.base;

/**
 * Created by Yurchenko on 26.10.2016.
 */

public interface Presenter<V extends MvpView> {

    void attachView(V mvpView);

    void detachView();
}
