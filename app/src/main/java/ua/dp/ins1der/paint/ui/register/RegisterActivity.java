package ua.dp.ins1der.paint.ui.register;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import ua.dp.ins1der.paint.R;
import ua.dp.ins1der.paint.ui.base.BaseActivity;
import ua.dp.ins1der.paint.ui.main.MainActivity;
import ua.dp.ins1der.paint.util.ViewUtil;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Yurchenko on 28.10.2016.
 */

public class RegisterActivity extends BaseActivity implements RegisterMvpView {
    @Inject RegisterPresenter mRegisterPresenter;

    @BindView(R.id.ra_root) View root;
    @BindView(R.id.ra_et_email) EditText emailET;
    @BindView(R.id.ra_et_password) EditText passwordET;
    @BindView(R.id.ra_et_password_repeat) EditText passwordRepeatET;
    @BindView(R.id.ra_pb) ProgressBar progressBar;
    @BindView(R.id.ra_b_register) Button registerB;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, RegisterActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        activityComponent().inject(this);
        mRegisterPresenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRegisterPresenter.detachView();
    }

    @Override
    public String getEmail() {
        return emailET.getText().toString();
    }

    @Override
    public String getPassword() {
        return passwordET.getText().toString();
    }

    @Override
    public String getPasswordRepeat() {
        return passwordRepeatET.getText().toString();
    }

    @OnClick(R.id.ra_b_register)
    @Override
    public void onRegisterClick() {
        mRegisterPresenter.onRegisterClick();
    }

    @Override
    public void showPasswordsNotMatch() {
        ViewUtil.showSnackbar(root, R.string.passwords_not_match);
    }

    @Override
    public void startMainActivity() {
        startActivity(MainActivity.getStartIntent(this));
        finish();
    }

    @Override
    public void showRegisterFailed(String errorText) {
        ViewUtil.showSnackbar(root, errorText);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
        registerB.setVisibility(View.INVISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.INVISIBLE);
        registerB.setVisibility(View.VISIBLE);
    }
}
