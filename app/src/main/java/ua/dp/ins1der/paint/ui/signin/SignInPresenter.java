package ua.dp.ins1der.paint.ui.signin;

import ua.dp.ins1der.paint.data.DataManager;
import ua.dp.ins1der.paint.data.remote.model.SignInRequest;
import ua.dp.ins1der.paint.ui.base.Presenter;
import ua.dp.ins1der.paint.util.EncodingUtil;
import ua.dp.ins1der.paint.util.TextUtils;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;

import javax.inject.Inject;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by Yurchenko on 28.10.2016.
 */

public class SignInPresenter implements Presenter<SignInMvpView> {
    private SignInMvpView mSignInMvpView;

    private final DataManager mDataManager;
    private Subscription mSubscription;

    @Inject public SignInPresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    @Override
    public void attachView(SignInMvpView mvpView) {
        mSignInMvpView = mvpView;
    }

    @Override
    public void detachView() {
        mSignInMvpView= null;
        if(mSubscription != null) {
            mSubscription.unsubscribe();
        }
    }

    public void onSigninClick() {
        String email = mSignInMvpView.getEmail();
        String password = mSignInMvpView.getPassword();
        if(TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            mSignInMvpView.signinFail(ua.dp.ins1der.paint.R.string.signin_error);
            return;
        }
        mSubscription = mDataManager.signin(new SignInRequest(email, EncodingUtil.toMD5(password)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        signInResponse -> mSignInMvpView.startMainActivity(),
                        e -> mSignInMvpView.signinFail(ua.dp.ins1der.paint.R.string.signin_error)
                );
    }

    public void handleGoogleSignResult(GoogleSignInResult googleSignInResult) {
        if(!googleSignInResult.isSuccess()) {
            mSignInMvpView.signinFail(ua.dp.ins1der.paint.R.string.signin_error);
            return;
        }
        String googlePass = EncodingUtil.toMD5("yurchenko");
        GoogleSignInAccount account = googleSignInResult.getSignInAccount();
        mSubscription = mDataManager.signin(new SignInRequest(account.getEmail(), googlePass))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        signInResponse -> mSignInMvpView.startMainActivity(),
                        (e) -> {
                            if(e instanceof HttpException) {
                                Timber.e("response code " + ((HttpException) e).response().code());
                            }
                            mSignInMvpView.signinFail(ua.dp.ins1der.paint.R.string.signin_error);
                        },
                        () -> {});
        ;
    }
}
