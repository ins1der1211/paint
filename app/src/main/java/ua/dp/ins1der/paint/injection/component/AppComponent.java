package ua.dp.ins1der.paint.injection.component;

import android.app.Application;
import android.content.Context;

import ua.dp.ins1der.paint.PaintApp;
import ua.dp.ins1der.paint.data.DataManager;
import ua.dp.ins1der.paint.data.local.DatabaseHelper;
import ua.dp.ins1der.paint.data.local.PreferencesHelper;
import ua.dp.ins1der.paint.data.remote.PaintService;
import ua.dp.ins1der.paint.data.remote.SyncService;
import ua.dp.ins1der.paint.injection.ApplicationContext;
import ua.dp.ins1der.paint.injection.module.AppModule;
import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Component;
import ua.dp.ins1der.paint.injection.module.SyncModule;

/**
 * Created by Yurchenko on 26.10.2016.
 */

@Singleton
@Component(modules = {
        AppModule.class,
        SyncModule.class
})
public interface AppComponent {
    void inject(PaintApp paintApp);
    void inject(SyncService syncService);

    @ApplicationContext
    Context context();
    Application application();
    PaintService paintService();
    PreferencesHelper preferencesHelper();
    DatabaseHelper databaseHelper();
    DataManager dataManager();
    Bus eventBus();
}
