package ua.dp.ins1der.paint.util;

import android.support.annotation.Nullable;

/**
 * Created by Yurchenko on 18.11.2016.
 */

/* Because android.TextUtils not work in unit test */

public class TextUtils {
    public static boolean isEmpty(@Nullable CharSequence str) {
        if (str == null || str.length() == 0)
            return true;
        else
            return false;
    }
}
