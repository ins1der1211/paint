package ua.dp.ins1der.paint.data.local;

import android.database.Cursor;

import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.SqlBrite;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.Subscriber;
import ua.dp.ins1der.paint.data.local.model.Image;

/**
 * Created by Yurchenko on 26.10.2016.
 */
@Singleton
public class DatabaseHelper {
    private final BriteDatabase mDb;

    @Inject
    public DatabaseHelper(DbOpenHelper dbOpenHelper) {
        mDb = SqlBrite.create().wrapDatabaseHelper(dbOpenHelper);
    }

    public BriteDatabase getBriteDb() {
        return mDb;
    }

    public Observable<Void> clearTables() {
        return Observable.create(new Observable.OnSubscribe<Void>() {
            @Override
            public void call(Subscriber<? super Void> subscriber) {
                BriteDatabase.Transaction transaction = mDb.newTransaction();
                try {
                    Cursor cursor = mDb.query("SELECT name FROM sqlite_master WHERE type='table'");
                    while (cursor.moveToNext()) {
                        mDb.delete(cursor.getString(cursor.getColumnIndex("name")), null);
                    }
                    cursor.close();
                    transaction.markSuccessful();
                    subscriber.onCompleted();
                } finally {
                    transaction.end();
                }
            }
        });
    }

    public Observable<Long> saveBitmap(Image image) {
        return Observable.create(subscriber -> {
            BriteDatabase.Transaction transaction = mDb.newTransaction();
            try {
                subscriber.onNext(mDb.insert(Db.ImagesTable.TABLE_NAME, Db.ImagesTable.toContentValues(image)));
                subscriber.onCompleted();
                transaction.markSuccessful();
            } finally {
                transaction.end();
            }
        });
    }

    public Observable<Image> getImageList() {
        return Observable.create(subscriber -> {
            BriteDatabase.Transaction transaction = mDb.newTransaction();
            try {
                Cursor cursor = mDb.query("SELECT * FROM " + Db.ImagesTable.TABLE_NAME +
                        " WHERE " + Db.ImagesTable.COLUMN_DELETED + " != ? ", "1");
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    subscriber.onNext(Db.ImagesTable.populateContent(cursor));
                    cursor.moveToNext();
                }
                cursor.close();
                subscriber.onCompleted();
                transaction.markSuccessful();
            } finally {
                transaction.end();
            }
        });
    }

    public Observable<Integer> markAsSync(Image image) {
        return Observable.create(subscriber -> {
            BriteDatabase.Transaction transaction = mDb.newTransaction();
            try {
                image.setSync(0);
                subscriber.onNext(mDb.update(Db.ImagesTable.TABLE_NAME, Db.ImagesTable.toContentValues(image),
                        Db.ImagesTable.COLUMN_IMAGE + " =? ", image.getImagePath()));
                subscriber.onCompleted();
                transaction.markSuccessful();
            } finally {
                transaction.end();
            }
        });
    }

    public Observable<Integer> markAsDeleted(Image image) {
        return Observable.create(subscriber -> {
            BriteDatabase.Transaction transaction = mDb.newTransaction();
            try {
                image.setDeleted(1);
                subscriber.onNext(mDb.update(Db.ImagesTable.TABLE_NAME, Db.ImagesTable.toContentValues(image),
                        Db.ImagesTable.COLUMN_IMAGE + " = ? ", image.getImagePath()));
                subscriber.onCompleted();
                transaction.markSuccessful();
            } finally {
                transaction.end();
            }
        });
    }

    public Observable<Image> getNotSyncImages() {
        return Observable.create(subscriber -> {
            BriteDatabase.Transaction transaction = mDb.newTransaction();
            try {
                Cursor cursor = mDb.query("SELECT * " +
                                " FROM " + Db.ImagesTable.TABLE_NAME +
                                " WHERE " + Db.ImagesTable.COLUMN_SYNC + " =? ", "1");
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    subscriber.onNext(Db.ImagesTable.populateContent(cursor));
                    cursor.moveToNext();
                }
                subscriber.onCompleted();
                transaction.markSuccessful();
                cursor.close();
            } finally {
                transaction.end();
            }
        });
    }

    public Observable<Integer> delete(Image image) {
        return Observable.create(subscriber -> {
            BriteDatabase.Transaction transaction = mDb.newTransaction();
            try {
                subscriber.onNext(mDb.delete(Db.ImagesTable.TABLE_NAME, Db.ImagesTable.COLUMN_SERVER_ID + " =? ",
                        image.getServerId()));
                subscriber.onCompleted();
                transaction.markSuccessful();
            } finally {
                transaction.end();
            }
        });
    }
}
