package ua.dp.ins1der.paint.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import butterknife.BindView;
import butterknife.OnClick;
import ua.dp.ins1der.paint.R;
import ua.dp.ins1der.paint.ui.base.BaseActivity;
import ua.dp.ins1der.paint.ui.gallery.GalleryActivity;
import ua.dp.ins1der.paint.ui.paint.PaintActivity;
import ua.dp.ins1der.paint.ui.welcome.WelcomeActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class MainActivity extends BaseActivity
        implements MainMvpView, NavigationView.OnNavigationItemSelectedListener {

    public static Intent getStartIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Inject MainPresenter mainPresenter;

    @BindView(R.id.nav_view) NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        activityComponent().inject(this);
        mainPresenter.attachView(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onDestroy() {
        mainPresenter.detachView();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.nav_paint:
                startPaint();
                break;
            case R.id.nav_gallery:
                startGallery();
                break;
            case R.id.nav_log_out:
                logout();
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @OnClick(R.id.start_paint_cv)
    public void onPaintClick() {
        startPaint();
    }

    @OnClick(R.id.start_gallery_cv)
    public void onGalleryClick() {
        startGallery();
    }

    @OnClick(R.id.logout_cv)
    public void onLogoutClick() {
        logout();
    }

    void startPaint() {
        startActivity(PaintActivity.getStartIntent(this));
    }

    void startGallery() {
        startActivity(GalleryActivity.getStartIntent(this));
    }

    void logout() {
        mainPresenter.logOut();
        startActivity(WelcomeActivity.getStartIntent(this));
        finish();
    }
}
