package ua.dp.ins1der.paint.ui.widgets;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

import ua.dp.ins1der.paint.ui.widgets.parcels.Layer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yurchenko on 27.10.2016.
 */

public class DrawingView extends View {
    /* Double click handling */
    private DoubleClickListener mDoubleClickListener;
    private static final long DOUBLE_CLICK_TIME_DELTA = 300;
    private long lastClickTime = 0;

    /* Drawing */
    private int mColor = Color.BLACK;
    private float mThickness = 5f;
    private List<Layer> mLayerList;
    private List<Bitmap> mBitmapList;
    private float mX1, mY1;
    private static  float TOLERANCE = 5f;
    private static int AVALAIBLE_LAYERS = 4;

    /* Scale  */
    private ScaleGestureDetectorFixed mGestureDetector;
    private ScaleListener mScaleListener;
    private float mScale = 1f;
    private boolean mAfterScale;

    public DrawingView(Context context) {
        this(context, null);
    }

    public DrawingView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DrawingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFocusable(true);
        setFocusableInTouchMode(true);
        mScaleListener = new ScaleListener();
        mGestureDetector = new ScaleGestureDetectorFixed(context, mScaleListener);
        setDrawingCacheEnabled(true);
        initLayers();
    }

    private void initLayers() {
        mLayerList = new ArrayList<>();
        mBitmapList = new ArrayList<>();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        MotionEvent e = MotionEvent.obtain(event);
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                /* double click*/
                long clickTime = System.currentTimeMillis();
                if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA && mDoubleClickListener != null)
                    mDoubleClickListener.onDoubleClick();
                else {
                    /* draw */
                    mX1 = e.getX();
                    mY1 = e.getY();
                    addBitmap();
                    mLayerList.add(new Layer(mColor, mThickness));
                    lastLayer().moveTo(mX1, mY1);
                }
                mAfterScale = false;
                lastClickTime = clickTime;
                break;
            case MotionEvent.ACTION_MOVE:
                /* draw */
                if(event.getPointerCount() == 1 && !mAfterScale) {
                    if(Math.abs(e.getX() - mX1) >= TOLERANCE
                            || Math.abs(e.getY() - mY1) >= TOLERANCE) {
                        lastLayer().quadTo(mX1, mY1, (e.getX() + mX1) / 2, (e.getY() + mY1) / 2);
                        mX1 = e.getX();
                        mY1 = e.getY();
                    }
                }
                /* scale */
                else mGestureDetector.onTouchEvent(event);
                break;
        }
        e.recycle();
        postInvalidate();
        return true;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if(!mBitmapList.isEmpty()) {
            canvas.drawBitmap(lastBitmap(), 0, 0, null);
        }
        for(Layer l : mLayerList) {
            canvas.drawPath(l.getPath(), l.getPaint());
        }
    }


    /* Scaling */
    public class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            mScale *= detector.getScaleFactor();
            if(mScale > 0.5f && mScale < 3f) {
                setScaleX(mScale);
                setScaleY(mScale);
            }
            mAfterScale = true;
            return true;
        }
    }

    public void setDefaultScale() {
        mScale = 1f;
        setScaleX(mScale);
        setScaleY(mScale);
    }


    /* Drawing */
    public void setColor(int color) {
        mColor = color;
    }

    public void setThickness(float thickness) {
        mThickness = thickness;
    }

    public void setEraser() {
        mColor = Color.WHITE;
        mThickness = 100f;
    }

    private Layer lastLayer() {
        return mLayerList.get(mLayerList.size() - 1);
    }

    public List<Layer> getLayerList() {
        return mLayerList;
    }

    public void setLayerList(List<Layer> layerList) {
        mLayerList = layerList;
        postInvalidate();
    }

    public List<Bitmap> getBitmapList() {
        return mBitmapList;
    }

    public void setBitmapList(List<Bitmap> bitmapList) {
        mBitmapList = bitmapList;
    }

    public Bitmap lastBitmap() {
        return mBitmapList.get(mBitmapList.size() - 1);
    }

    private void addBitmap() {
        if(mLayerList.size() > AVALAIBLE_LAYERS) {
            mLayerList.remove(0);
            mBitmapList.remove(0);
        }
        buildDrawingCache();
        mBitmapList.add(Bitmap.createBitmap(getDrawingCache()));
        destroyDrawingCache();
    }

    public void rollBack() {
        if(!mLayerList.isEmpty()) {
            if(mLayerList.size() > 1) mBitmapList.remove(mBitmapList.size() - 1);
            mLayerList.remove(mLayerList.size() - 1);
            postInvalidate();
        }
    }


    /* Double click */
    public interface DoubleClickListener {
        void onDoubleClick();
    }

    public void setDoubleClickListener(DoubleClickListener doubleClickListener) {
        mDoubleClickListener = doubleClickListener;
    }
}
