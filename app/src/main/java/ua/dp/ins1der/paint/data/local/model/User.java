package ua.dp.ins1der.paint.data.local.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Yurchenko on 08.12.2016.
 */

public class User {

    public User() {}

    public User(String token, int userId) {
        this.token = token;
        this.userId = userId;
    }

    @SerializedName("token")
    private String token;

    @SerializedName("user_id")
    private int userId;

    public String getToken() {
        return token;
    }

    public int getUserId() {
        return userId;
    }
}
