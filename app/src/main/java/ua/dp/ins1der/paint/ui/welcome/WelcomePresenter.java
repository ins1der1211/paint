package ua.dp.ins1der.paint.ui.welcome;

import ua.dp.ins1der.paint.ui.base.Presenter;

import javax.inject.Inject;

/**
 * Created by Yurchenko on 27.10.2016.
 */

public class WelcomePresenter implements Presenter<WelcomeMvpView> {
    private WelcomeMvpView mMvpView;

    @Inject
    public WelcomePresenter() {}

    @Override
    public void attachView(WelcomeMvpView mvpView) {
        mMvpView = mvpView;
    }

    @Override
    public void detachView() {
        mMvpView = null;
    }
}
