package ua.dp.ins1der.paint.ui.signin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.view.View;
import android.widget.EditText;

import ua.dp.ins1der.paint.R;
import ua.dp.ins1der.paint.ui.base.BaseActivity;
import ua.dp.ins1der.paint.ui.main.MainActivity;
import ua.dp.ins1der.paint.util.ViewUtil;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Yurchenko on 28.10.2016.
 */

public class SignInActivity extends BaseActivity implements SignInMvpView, GoogleApiClient.OnConnectionFailedListener {
    private static final int RC_SIGN_IN = 1;

    @Inject SignInPresenter mSignInPresenter;

    @BindView(R.id.root) View root;
    @BindView(R.id.sa_et_email) EditText emailET;
    @BindView(R.id.sa_et_password) EditText passwordET;

    GoogleApiClient mGoogleApiClient;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, SignInActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        ButterKnife.bind(this);
        activityComponent().inject(this);
        mSignInPresenter.attachView(this);
        createGoogleApiClient();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ViewUtil.hideKeyboard(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSignInPresenter.detachView();
    }

    @Override
    public String getEmail() {
        return emailET.getText().toString();
    }

    @Override
    public String getPassword() {
        return passwordET.getText().toString();
    }

    @OnClick(R.id.sa_b_sign_in)
    @Override
    public void onSignInClick() {
        mSignInPresenter.onSigninClick();
    }

    @OnClick(R.id.sa_iv_google_sign)
    @Override
    public void onGoogleSignClick() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void signinFail(@StringRes int errorText) {
        ViewUtil.showSnackbar(root, errorText);
    }

    @Override
    public void startMainActivity() {
        startActivity(MainActivity.getStartIntent(this));
        finish();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        ViewUtil.showSnackbar(root, connectionResult.getErrorMessage());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            mSignInPresenter.handleGoogleSignResult(Auth.GoogleSignInApi.getSignInResultFromIntent(data));
        }
    }

    private void createGoogleApiClient() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    @OnClick(R.id.for_test)
    public void forTest() {
        startMainActivity();
    }
}
