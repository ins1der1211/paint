package ua.dp.ins1der.paint.ui.gallery;

import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;
import ua.dp.ins1der.paint.R;

/**
 * Created by Yurchenko on 26.11.2016.
 */

public class PicturePagerAdapter extends PagerAdapter {

    List<Bitmap> mBitmapList;

    public PicturePagerAdapter(List<Bitmap> bitmapList) {
        mBitmapList = bitmapList;
    }

    @Override
    public int getCount() {
        return mBitmapList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(container.getContext());
        View view = inflater.inflate(R.layout.pager_picture, container, false);
        ((ImageView)view.findViewById(R.id.picture)).setImageBitmap(mBitmapList.get(position));
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View)object);
    }
}
