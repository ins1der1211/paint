package ua.dp.ins1der.paint.ui.paint;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Parcelable;

import java.util.ArrayList;

import ua.dp.ins1der.paint.data.DataManager;
import ua.dp.ins1der.paint.ui.base.Presenter;

import javax.inject.Inject;

/**
 * Created by Yurchenko on 27.10.2016.
 */

public class PaintPresenter implements Presenter<PaintMvpView> {

    private PaintMvpView mPaintMvpView;

    public static final String LAYER_LIST = "layer_list";
    public static final String BITMAP_LIST = "bitmap_list";
    public static final String LAST_COLOR = "last_color";
    public static final String LAST_THICKNESS = "last_thickness";

    private final DataManager mDataManager;

    @Inject PaintPresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    @Override
    public void attachView(PaintMvpView mvpView) {
        mPaintMvpView = mvpView;
    }

    @Override
    public void detachView() {
        mPaintMvpView = null;
    }

    public void savePaint(Bitmap bitmap) {
        mDataManager.saveBitmap(bitmap);
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(LAST_COLOR, mPaintMvpView.getLastColor());
        outState.putFloat(LAST_THICKNESS, mPaintMvpView.getLastThickness());
        outState.putParcelableArrayList(LAYER_LIST, (ArrayList<? extends Parcelable>) mPaintMvpView.getLayerList());
        outState.putParcelableArrayList(BITMAP_LIST, (ArrayList<? extends Parcelable>) mPaintMvpView.getBitmapList());
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        mPaintMvpView.setLayerList(savedInstanceState.getParcelableArrayList(LAYER_LIST));
        mPaintMvpView.setBitmapList(savedInstanceState.getParcelableArrayList(BITMAP_LIST));
        mPaintMvpView.setLastColor(savedInstanceState.getInt(LAST_COLOR));
        mPaintMvpView.setLastThickness(savedInstanceState.getFloat(LAST_THICKNESS));
    }
}
