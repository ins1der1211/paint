package ua.dp.ins1der.paint.data.local.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Yurchenko on 26.10.2016.
 */

public class Image implements Parcelable {

    @SerializedName("image_path")
    private String imagePath;

    @SerializedName("sync")
    private int sync;

    @SerializedName("deleted")
    private int deleted;

    @SerializedName("server_id")
    private String serverId;

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String image) {
        this.imagePath = image;
    }

    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.imagePath);
        dest.writeInt(this.sync);
        dest.writeInt(this.deleted);
        dest.writeString(this.serverId);
    }

    public Image() {
    }

    protected Image(Parcel in) {
        this.imagePath = in.readString();
        this.sync = in.readInt();
        this.deleted = in.readInt();
        this.serverId = in.readString();
    }

    public static final Creator<Image> CREATOR = new Creator<Image>() {
        @Override
        public Image createFromParcel(Parcel source) {
            return new Image(source);
        }

        @Override
        public Image[] newArray(int size) {
            return new Image[size];
        }
    };
}
