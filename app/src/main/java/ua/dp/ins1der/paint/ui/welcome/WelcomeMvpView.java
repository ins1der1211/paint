package ua.dp.ins1der.paint.ui.welcome;

import ua.dp.ins1der.paint.ui.base.MvpView;

/**
 * Created by Yurchenko on 27.10.2016.
 */

public interface WelcomeMvpView extends MvpView {

    void onSignInClick();

    void onRegisterClick();
}
