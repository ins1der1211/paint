package ua.dp.ins1der.paint.ui.gallery;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import ua.dp.ins1der.paint.R;

/**
 * Created by Yurchenko on 24.11.2016.
 */

public class PictureRecyclerViewAdapter extends RecyclerView.Adapter<PictureRecyclerViewAdapter.ViewHolder> {

    private List<Bitmap> mBitmapList;
    private InteractionListrener mOnItemClickListener;

    public PictureRecyclerViewAdapter(List<Bitmap> bitmapList) {
        mBitmapList = bitmapList;
    }

    @Override
    public PictureRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_picture, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PictureRecyclerViewAdapter.ViewHolder holder, int position) {
        holder.picture.setImageBitmap(mBitmapList.get(position));
    }

    @Override
    public int getItemCount() {
        return mBitmapList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.picture) ImageView picture;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.picture)
        public void click() {
            if(mOnItemClickListener != null) mOnItemClickListener.onItemClick(getAdapterPosition());
        }

        @OnLongClick(R.id.picture)
        public boolean longClick() {
            if(mOnItemClickListener != null) mOnItemClickListener.onLongItemClick(getAdapterPosition());
            return true;
        }
    }

    public void setOnItemClickListener(InteractionListrener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    public interface InteractionListrener {
        void onItemClick(int position);
        void onLongItemClick(int position);
    }
}