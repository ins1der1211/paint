package ua.dp.ins1der.paint.ui.gallery;

import android.graphics.Bitmap;

import java.util.List;

import ua.dp.ins1der.paint.ui.base.MvpView;

/**
 * Created by Yurchenko on 24.11.2016.
 */

public interface GalleryMvpView extends MvpView {

    void initRecyclerView(List<Bitmap> bitmapList);

    void initViewPager(List<Bitmap> bitmapList);

    boolean pagerOpen();

    boolean gridType();

    int pagerPosition();

    void toGrid();

    void toList();

    void openPagerOnPosition(int position);

    void showPagerMenu();

    void showRecyclerMenu();

}
