package ua.dp.ins1der.paint.data.remote.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Yurchenko on 28.10.2016.
 */

public class SignInRequest extends BaseRequest {
    @SerializedName("email")
    private String email;

    @SerializedName("password")
    private String password;

    public SignInRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
