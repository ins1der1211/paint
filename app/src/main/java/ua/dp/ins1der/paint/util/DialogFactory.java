package ua.dp.ins1der.paint.util;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.StringRes;

/**
 * Created by Yurchenko on 26.10.2016.
 */

public class DialogFactory {
    public static Dialog createSimpleOkErrorDialog(Context context, String title, String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setNeutralButton(android.R.string.ok, null);
        return alertDialog.create();
    }

    public static Dialog createSimpleOkErrorDialog(Context context,
                                                   @StringRes int titleResource,
                                                   @StringRes int messageResource) {

        return createSimpleOkErrorDialog(context,
                context.getString(titleResource),
                context.getString(messageResource));
    }

    public static Dialog createSimpleOkErrorDialog(Context context, String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context)
                .setTitle(context.getString(android.R.string.dialog_alert_title))
                .setMessage(message)
                .setNeutralButton(android.R.string.ok, null);
        return alertDialog.create();
    }

    public static Dialog createSimpleOkErrorDialog(Context context,
                                                   @StringRes int messageResource) {

        return createSimpleOkErrorDialog(context, context.getString(messageResource));
    }

    public static Dialog createSimpleOkErrorDialog(Context context,
                                                   String title,
                                                   String message,
                                                   DialogInterface.OnClickListener positiveClickListener,
                                                   DialogInterface.OnClickListener negativeClickListener) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, positiveClickListener)
                .setNegativeButton(android.R.string.cancel, negativeClickListener);
        return alertDialog.create();
    }

    public static Dialog createSimpleOkErrorDialog(Context context,
                                                   @StringRes int titleResource,
                                                   @StringRes int messageResource,
                                                   DialogInterface.OnClickListener positiveClickListener,
                                                   DialogInterface.OnClickListener negativeClickListener) {
        return createSimpleOkErrorDialog(context,
                context.getString(titleResource),
                context.getString(messageResource),
                positiveClickListener,
                negativeClickListener);
    }
}
