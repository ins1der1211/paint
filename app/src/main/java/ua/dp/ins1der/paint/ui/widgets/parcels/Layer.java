package ua.dp.ins1der.paint.ui.widgets.parcels;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Parcel;
import android.os.Parcelable;

import ua.dp.ins1der.paint.ui.widgets.parcels.path.ParcelablePath;

/**
 * Created by Yurchenko on 07.11.2016.
 */

public class Layer implements Parcelable {
    private ParcelablePath mPath;
    private ParcelablePaint mPaint;

    private int mColor;
    private float mStrokeWidth;

    public Layer() {
        mColor = Color.BLACK;
        mStrokeWidth = 5f;
        setupPaint();
    }

    public Layer(int color) {
        mColor = color;
        setupPaint();
    }

    public Layer(int color, float strokeWidth) {
        mColor = color;
        mStrokeWidth = strokeWidth;
        setupPaint();
    }

    private void setupPaint() {
        mPath = new ParcelablePath();
        mPaint = new ParcelablePaint();
        mPaint.setColor(mColor);
        mPaint.setAntiAlias(true);
        mPaint.setStrokeWidth(mStrokeWidth);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.BUTT);
    }

    public Path getPath() {
        return mPath;
    }

    public Paint getPaint() {
        return mPaint;
    }

    public void moveTo(float x, float y) {
        mPath.moveTo(x, y);
    }

    public void quadTo(float x, float y, float x2, float y2) {
        mPath.quadTo(x, y, x2, y2);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.mPath, flags);
        dest.writeParcelable(this.mPaint, flags);
        dest.writeInt(this.mColor);
        dest.writeFloat(this.mStrokeWidth);
    }

    protected Layer(Parcel in) {
        this.mPath = in.readParcelable(ParcelablePath.class.getClassLoader());
        this.mPaint = in.readParcelable(ParcelablePaint.class.getClassLoader());
        this.mColor = in.readInt();
        this.mStrokeWidth = in.readFloat();
    }

    public static final Parcelable.Creator<Layer> CREATOR = new Parcelable.Creator<Layer>() {
        @Override
        public Layer createFromParcel(Parcel source) {
            return new Layer(source);
        }

        @Override
        public Layer[] newArray(int size) {
            return new Layer[size];
        }
    };
}
