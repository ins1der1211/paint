package ua.dp.ins1der.paint.ui.widgets.parcels.path;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Yurchenko on 08.11.2016.
 */

public class ParcelableMove implements Parcelable {
    private float x,y;

    public ParcelableMove(float x, float y){
        this.x = x;
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(this.x);
        dest.writeFloat(this.y);
    }

    protected ParcelableMove(Parcel in) {
        this.x = in.readFloat();
        this.y = in.readFloat();
    }

    public static final Parcelable.Creator<ParcelableMove> CREATOR = new Parcelable.Creator<ParcelableMove>() {
        @Override
        public ParcelableMove createFromParcel(Parcel source) {
            return new ParcelableMove(source);
        }

        @Override
        public ParcelableMove[] newArray(int size) {
            return new ParcelableMove[size];
        }
    };
}
