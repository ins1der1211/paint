package ua.dp.ins1der.paint.data.local;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;

import rx.Observable;
import ua.dp.ins1der.paint.data.DataManager;
import ua.dp.ins1der.paint.util.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Yurchenko on 15.11.2016.
 */

@Singleton
public class FileHelper {

    @Inject FileUtils mFileUtils;

    @Inject FileHelper() {}

    public Observable<File> saveBitmap(@NonNull Bitmap b) {
        FileOutputStream fOut = null;
        File savedFile = null;
        try {
            savedFile = mFileUtils.getFileToSave();
            fOut = new FileOutputStream(savedFile);
            b.compress(Bitmap.CompressFormat.PNG, 100, fOut);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if(fOut != null) fOut.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        File finalSavedFile = savedFile;
        return Observable.create(s -> {
            s.onNext(finalSavedFile);
            s.onCompleted();
        });
    }

    public Observable<Boolean> deleteFile(File file) {
        return Observable.create(s -> {
            s.onNext(file.delete());
            s.onCompleted();
        });
    }

}
