package ua.dp.ins1der.paint.data.remote;

import android.support.design.BuildConfig;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.http.DELETE;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.Part;
import retrofit2.http.Path;
import ua.dp.ins1der.paint.data.remote.model.RegisterRequest;
import ua.dp.ins1der.paint.data.remote.model.RegisterResponse;
import ua.dp.ins1der.paint.data.remote.model.SignInRequest;
import ua.dp.ins1der.paint.data.remote.model.SignInResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;
import ua.dp.ins1der.paint.data.remote.model.UploadResponse;

/**
 * Created by Yurchenko on 26.10.2016.
 */

public interface PaintService {
    String ENDPOINT = "http://nikita.adscab.eu/api/";

    @POST("register")
    Observable<RegisterResponse> register(@Body RegisterRequest registerRequest);

    @POST("signin")
    Observable<SignInResponse> signin(@Body SignInRequest signInRequest);

    @Multipart
    @POST("upload")
    Observable<UploadResponse> upload(@Part MultipartBody.Part file);

    @DELETE("delete" + "/{server_id}")
    Observable<ResponseBody> delete(@Path("server_id") String serverId);

    class Factory {
        public static PaintService makePaintService(String baseUrl) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .build();

            Gson gson = new GsonBuilder().create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();

            return retrofit.create(PaintService.class);
        }
    }

    class Util {
        public static String buildAuth(String accessToken) {
            return "Bearer " + accessToken;
        }
    }
}
