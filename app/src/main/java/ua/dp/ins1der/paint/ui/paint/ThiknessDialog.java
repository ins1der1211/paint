package ua.dp.ins1der.paint.ui.paint;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yurchenko on 08.11.2016.
 */

public class ThiknessDialog extends AlertDialog {

    @BindView(ua.dp.ins1der.paint.R.id.seek_bar) SeekBar seekBar;
    @BindView(ua.dp.ins1der.paint.R.id.seek_value_tv) TextView seekBarValueTV;

    private OnSeekListener onSeekListener;

    protected ThiknessDialog(Context context, int initValue, OnSeekListener onSeekListener) {
        super(context);
        this.onSeekListener = onSeekListener;
        View root = getLayoutInflater().inflate(ua.dp.ins1der.paint.R.layout.dialog_choose_thickness, null);
        ButterKnife.bind(this, root);
        seekBar.setProgress(initValue);
        seekBar.setOnSeekBarChangeListener(seekBarChangeListener);
        seekBarValueTV.setText(String.valueOf(seekBar.getProgress()));
        setButton(BUTTON_POSITIVE, context.getString(android.R.string.ok), clickListener);
        setButton(BUTTON_NEGATIVE, context.getString(android.R.string.cancel), clickListener);
        setView(root);
    }

    SeekBar.OnSeekBarChangeListener seekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            seekBarValueTV.setText(String.valueOf(progress));
        }
        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {}
        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            if(seekBar.getProgress() < 1) seekBar.setProgress(1);
        }
    };

    private DialogInterface.OnClickListener clickListener = (dialog, which) -> {
        switch (which) {
            case BUTTON_POSITIVE:
                onSeekListener.onThicknessSelected(seekBar.getProgress());
                break;
            case BUTTON_NEGATIVE:
                dialog.dismiss();
                break;
        }
    };

    interface OnSeekListener {
        void onThicknessSelected(float thickness);
    }
}
