package ua.dp.ins1der.paint.injection.module;

import android.app.Application;
import android.content.Context;

import ua.dp.ins1der.paint.data.remote.PaintService;
import ua.dp.ins1der.paint.data.remote.SyncService;
import ua.dp.ins1der.paint.injection.ApplicationContext;

import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.PeriodicTask;
import com.google.android.gms.gcm.Task;
import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Yurchenko on 26.10.2016.
 */
@Module
public class AppModule {

    protected final Application mApplication;

    public AppModule(Application application) {
        mApplication = application;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    Bus provideEventBus() {
        return new Bus();
    }

    @Provides
    @Singleton
    PaintService providePaintService() {
        return PaintService.Factory.makePaintService(PaintService.ENDPOINT);
    }
}
