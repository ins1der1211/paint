package ua.dp.ins1der.paint.ui.main;

import ua.dp.ins1der.paint.data.DataManager;
import ua.dp.ins1der.paint.ui.base.Presenter;

import javax.inject.Inject;

/**
 * Created by Yurchenko on 17.11.2016.
 */

public class MainPresenter implements Presenter<MainMvpView> {
    private MainMvpView mMainMvpView;

    private final DataManager mDataManager;

    @Inject public MainPresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    @Override
    public void attachView(MainMvpView mvpView) {
        mMainMvpView = mvpView;
    }

    @Override
    public void detachView() {
        mMainMvpView = null;
    }

    public void logOut() {
        mDataManager.logOut();
    }
}
