package ua.dp.ins1der.paint.ui.widgets.parcels.path;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Yurchenko on 08.11.2016.
 */

public class ParcelableQuad implements Parcelable {
    private float x,y,x1,y1;

    public ParcelableQuad(float x, float y, float x1, float y1){
        this.x = x;
        this.y = y;
        this.x1 = x1;
        this.y1 = y1;
    }

    public float getY1() {
        return y1;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getX1() {
        return x1;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(this.x);
        dest.writeFloat(this.y);
        dest.writeFloat(this.x1);
        dest.writeFloat(this.y1);
    }

    protected ParcelableQuad(Parcel in) {
        this.x = in.readFloat();
        this.y = in.readFloat();
        this.x1 = in.readFloat();
        this.y1 = in.readFloat();
    }

    public static final Parcelable.Creator<ParcelableQuad> CREATOR = new Parcelable.Creator<ParcelableQuad>() {
        @Override
        public ParcelableQuad createFromParcel(Parcel source) {
            return new ParcelableQuad(source);
        }

        @Override
        public ParcelableQuad[] newArray(int size) {
            return new ParcelableQuad[size];
        }
    };
}
