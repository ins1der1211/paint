package ua.dp.ins1der.paint.ui.register;

import ua.dp.ins1der.paint.ui.base.MvpView;

/**
 * Created by Yurchenko on 28.10.2016.
 */

public interface RegisterMvpView extends MvpView {

    String getEmail();

    String getPassword();

    String getPasswordRepeat();

    void onRegisterClick();

    void showPasswordsNotMatch();

    void startMainActivity();

    void showRegisterFailed(String errorText);

    void showProgress();

    void hideProgress();
}
