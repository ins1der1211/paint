package ua.dp.ins1der.paint.data.remote.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Yurchenko on 28.10.2016.
 */

public class RegisterRequest extends BaseRequest {
    @SerializedName("email")
    private String email;

    @SerializedName("password")
    private String password;

    @SerializedName("password_repeat")
    private String passwordRepeat;

    public RegisterRequest() {}

    public RegisterRequest(String email, String password, String passwordAgain) {
        this.email = email;
        this.password = password;
        this.passwordRepeat = passwordAgain;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordRepeat() {
        return passwordRepeat;
    }

    public void setPasswordRepeat(String passwordRepeat) {
        this.passwordRepeat = passwordRepeat;
    }
}
