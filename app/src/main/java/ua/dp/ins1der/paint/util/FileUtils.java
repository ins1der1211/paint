package ua.dp.ins1der.paint.util;

import android.app.Application;
import android.os.Environment;

import java.io.File;

import javax.inject.Inject;

/**
 * Created by Yurchenko on 15.11.2016.
 */

public class FileUtils {
    private static final String APP_FOLDER = "Paint";

    private Application app;

    @Inject
    public FileUtils(Application app) {
        this.app = app;
    }

    public void createAppDir() {
        File file = new File(getAppFolder());
        if(!file.exists())
            file.mkdir();
    }

    public String getAppFolder() {
        String path;
        File rootDir = app.getFilesDir();
        path = rootDir.getAbsolutePath() + "/" + APP_FOLDER;
        return path;
    }

    public File getFileToSave() {
        return new File(getAppFolder() + "/" + System.currentTimeMillis() + ".png");
    }
}
