package ua.dp.ins1der.paint.data.remote;

import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.TaskParams;

import javax.inject.Inject;

import ua.dp.ins1der.paint.PaintApp;
import ua.dp.ins1der.paint.data.DataManager;

/**
 * Created by Yurchenko on 08.12.2016.
 */

public class SyncService extends GcmTaskService {

    @Inject DataManager mDataManager;

    @Override
    public void onCreate() {
        super.onCreate();
        PaintApp.get(this).getComponent().inject(this);
    }

    @Override
    public int onRunTask(TaskParams taskParams) {
        mDataManager.getNotSyncImages()
                .subscribe(
                       image -> {
                           if (image.getDeleted() == 1) mDataManager.deleteFromDb(image);
                           else mDataManager.upload(image);
                       }
                );
        return GcmNetworkManager.RESULT_RESCHEDULE;
    }
}
